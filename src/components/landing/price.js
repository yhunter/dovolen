//Шестой экран. Тарифы
import React from 'react';

function Price(props) {
    return(
        <section id="price" className="price">
                          <div className="wrapper">       
                                  <h2>Тарифы</h2>
                                  <div className="priceGrid">
                                    <div className="priceItem">
                                      <div className="priceHeader">
                                        Для бизнеса
                                      </div>
                                      <div className="priceBody">
                                        Бесплатно
                                      </div>
                                    </div>
                                    <div className="priceItem">
                                      <div className="priceHeader">
                                        Для агента
                                      </div>
                                      <div className="priceBody">
                                        —
                                      </div>
                                    </div>
                                    <div className="priceItem">
                                      <div className="priceHeader">
                                        Для получателя
                                      </div>
                                      <div className="priceBody">
                                        5%
                                      </div>
                                    </div>
                                  </div>
                          </div>       
                                  
        </section>
    );
}

export default Price;