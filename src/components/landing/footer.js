//Подвал лендинга
import React from 'react';

import logo from'../../assets/logo.svg'; //Логотип Dovolen
//Иконки социалок
import socialVk from '../../assets/landing/vk.svg';
import socialFb from '../../assets/landing/fb.svg';

function LandingFooter(props) {
    return(
        <footer className="footer">
                  <div className="wrapper">
                    <div className="footerBody">
                      <div className="footerLogo">
                                  <img src={logo} alt="dovolen.me" />
                                  
                      </div>
                      <div className="footerContacts">
                                  <h3>Контакты</h3>
                                  <a href="tel:+7123456789">+7-123-456-78-90</a>
                                  <a href="mailto:mail@dovolen.me">mail@dovolen.me</a>
                                  <div className="social">
                                    <a href="vk.com"><img src={socialVk} alt="VK" /></a>
                                    <a href="fb.com"><img src={socialFb} alt="FB" /></a>
                                  </div>
                      </div>
                      <div className="footerInfo">
                                  <h3>Информация</h3>
                                  <a href="/"> Публичная оферта</a>
                                  <a href="/"> Политика конфиденциальности</a>
                                  <a href="/"> Вопрос-ответ</a>
                      </div>
                      <div className="footerSupport">
                                  <h3>
                                    Поддержка
                                  </h3>
                                  <a href="/"> Написать в техподдержку</a>
                      </div>
                    </div>
                    <div className="footerDisclaimer">
                    Мы используем файлы cookies, чтобы вам было удобно работать с сайтом. Продолжая пользоваться сайтом, вы выражаете своё согласие на обработку ваших данных с использованием интернет-сервисов «Google Analytics» и «Яндекс Метрика». Порядок обработки ваших данных, а также реализуемые требования к их защите содержатся в&nbsp;<a href="/">Политике обработки данных</a>. В случае несогласия с обработкой ваших данных вы можете отключить сохранение cookie в настройках вашего браузера.
                    </div>
                  </div>
        </footer>
    );
}

export default LandingFooter;