//Второй экран. Для кого
import React, { useState } from 'react';

import {Grid} from '@material-ui/core';


const historyMenu = [ //Массив пунктов бокового меню
    'Рестораны, кафе и бары',
    'Салоны красоты',
    'Курьерские службы и службы доставки',
    'Отели и гостиницы',
    'Заправки',
    'Автомойки',
    'Фитнес-клубы',
    'Магазины косметики',
    'Медийные личности'
];

const ShowMenu = (props) => { //Формируем боковое меню из массива
    let output = [];
    let actClass = "";
    historyMenu.forEach(function(item, index, arr) {        
        actClass = index === props.active ? 'active' : ''; //Класс активному пункту меню
        output.push(<a href="/#" className={actClass} onClick={(e)=>{ e.preventDefault(); props.func(index);}} >{item}</a>);
    });
    return (
        output
    );
}

function Histories(props) { //Компонент истории
    
    const [activeHistory, showHistory] = useState(0); //Состояние активной истории


    const historyChange = (index) => { //Смена состояния истории
        showHistory(index);
    };   


    return(
      <section id="histories" className="histories">
            <div className="wrapper">                    
                    <Grid container spacing={0}>
                                    <Grid item xs={12} lg={4} className="historiesMenu">
                                        <h2>Для кого?</h2>
                                        {/*выводим список ссылок из массива */}
                                        <ShowMenu func={historyChange} active={activeHistory} />                            
                                    </Grid>
                                    <Grid item xs={12}  lg={8} className="historiesText">
                                      <div className="slider" style={{"width": historyMenu.length*100+"%", "transform": "translateX(-"+activeHistory*100/historyMenu.length+"%)"}}>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                          <div className="sliderItem">
                                            <p><img src="https://yhunter.ru/temp/dovolen/static/media/courtney-cook-le7D9QFiPr8-unsplash.d46ce5cf.jpg" alt="" className="pic alignRight" />Таким образом начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности играет важную роль в формировании системы обучения кадров, соответствует насущным потребностям. Равным образом постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки системы обучения кадров, соответствует насущным потребностям. Повседневная практика показывает, что постоянный количественный рост и сфера нашей активности способствует подготовки и реализации систем массового участия. Равным образом реализация намеченных плановых заданий играет важную роль в формировании существенных финансовых и административных условий. </p>
                                          </div>
                                      </div>                             
                                      {/*<ShowContent item={activeHistory} />*/} {/*выводим текст истории */}                          
                                    </Grid>
                    </Grid>
              </div>    
        </section> 
    );
}

export default Histories;