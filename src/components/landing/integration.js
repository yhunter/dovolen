//Пятый экран. Интеграция в фирменный стиль
import React from 'react';

//Картинки интеграций
import barber from '../../assets/landing/barber.jpg';
import cafe from '../../assets/landing/cafe.jpg';
import card from '../../assets/landing/card.jpg';
import check from '../../assets/landing/check.jpg';
import message from '../../assets/landing/message.jpg';
import app from '../../assets/landing/app.jpg';

function Integration(props) {
    return(
        <section id="integration" className="integration">
                    <div className="whiteBg">
                        <div className="wrapper">
                          
                            <h2>Интеграция в фирменный стиль</h2>
                            <div className="integrationGallery">
                                  <div className="integrationItem">
                                      <img src={barber} alt="" />
                                      <div className="caption">
                                          Наклейки
                                      </div>
                                  </div>
                                  <div className="integrationItem">
                                      <img src={cafe} alt="" />
                                      <div className="caption">
                                          Стойки
                                      </div>
                                  </div> 
                                  <div className="integrationItem">
                                      <img src={check} alt="" />
                                      <div className="caption">
                                          Кассовый чек
                                      </div>
                                  </div> 
                                  <div className="integrationItem">
                                      <img src={card} alt="" />
                                      <div className="caption">
                                          Карточка в счет
                                      </div>
                                  </div>
                                  <div className="integrationItem">
                                      <img src={message} alt="" />
                                      <div className="caption">
                                          Код в сообщении
                                      </div>
                                  </div> 
                                  <div className="integrationItem">
                                      <img src={app} alt="" />
                                      <div className="caption">
                                          Кнопка в приложении
                                      </div>
                                  </div>  
                                  
                                        
                            </div> 
                        </div> 
                    </div> 
                    <div className="whiteSmile">                      
                    </div>               
        </section>
    );
}

export default Integration;