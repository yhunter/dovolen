//Второй экран. Для кого
import React, { useState } from 'react';

import { Link as ScrollLink} from 'react-scroll';


const historyMenu = [ //Массив пунктов бокового меню
    [ 
        'Рестораны, кафе и бары',
        'Салоны красоты',
        'Курьерские службы и службы доставки',
        'Отели и гостиницы',
        'Заправки',
        'Автомойки',
        'Оффлайн-магазины',
    ],
    [ //Массив пунктов бокового меню
        'Медийные личности',
        'Блогеры',
        'Стриммеры',
        'Уличные музыканты',
        'Всем, кто копит на мечту'
    ]
];




const ShowMenu = (props) => { //Формируем боковое меню из массива
    let output = [];
    let actClass = "cat"+props.category;
    historyMenu[props.category].forEach(function(item, index, arr) {        
        //actClass = index === props.active ? 'active' : ''; //Класс активному пункту меню
        output.push(<ScrollLink to={props.link} smooth={true} offset={-100} duration={500} className={actClass} >{item}</ScrollLink>);
    });
    return (
        output
    );
}

function Histories(props) { //Компонент истории
    
    const [activeHistory, showHistory] = useState(0); //Состояние активной истории


    const historyChange = (index) => { //Смена состояния истории
        showHistory(index);
    };   


    return(
      <section id="histories" className="histories histories2">
            <div className="wrapper"> 
                <h2>Для кого?</h2>
                <div className="historiesMenu">                    
                    <ShowMenu func={historyChange}  category={0} link="business" />
                {/*</div>
                <div className="historiesMenu"> */}    
                    <ShowMenu func={historyChange}  category={1} link="blogers" />   
                </div>                       
              </div>    
        </section> 
    );
}

export default Histories;