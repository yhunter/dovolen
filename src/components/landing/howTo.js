//Третий экран. Как работает
import React from 'react';

//Иконки
import creditCard from '../../assets/landing/credit-cards.svg'; 
import rouble from '../../assets/landing/russian-rouble.svg';
import qr from '../../assets/landing/qrIcon.svg';
import registration from '../../assets/landing/registration.svg';

function HowTo(props) {
    return(
        <section id="howTo" className="howTo">
                    <div className="whiteSad">                     
                    </div>
                    <div className="whiteText">
                        <div className="wrapper" style={{width: "80%"}}>
                                <h2>Как работает сервис</h2>
                                <div className="stepper">
                                    <div className="stepperItem">
                                      <div className="icon">
                                        <img src={registration} alt="Регистрация" />
                                      </div>
                                      <div className="description">
                                        Получатель регистрируется в&nbsp;сервисе
                                      </div>
                                    </div>

                                    <div className="stepperItem">
                                    <div className="icon">
                                      <img src={qr} alt="Сканирование QR" />
                                    </div>
                                    <div className="description">
                                      Клиент сканирует QR или&nbsp;вводит код сотрудника
                                      

                                    </div>
                                    </div>

                                    <div className="stepperItem">
                                    <div className="icon">
                                      <img src={creditCard} alt="Вывод на карту" />
                                    </div>
                                      <div className="description">
                                      Клиент вводит сумму
                                      и&nbsp;подтверждает платеж

                                      </div>
                                    </div>

                                    <div className="stepperItem">
                                    <div className="icon">
                                      <img src={rouble} alt="Деньги" />
                                    </div>
                                      <div className="description">
                                        Деньги мгновенно зачисляются на&nbsp;карту

                                      </div>
                                    </div>

                                </div>
                                
                                
      
                        </div>
                    </div>
        </section>
    );
}

export default HowTo;