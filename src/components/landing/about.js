//Первый экран. О сервисе
import React from 'react';
import {Button} from '@material-ui/core';

import { Link as ScrollLink} from 'react-scroll';

import teaserPhone from '../../assets/landing/teaserPhone.png';
import teaserPrint from '../../assets/landing/teaserPrint.png';

function About(props) {
    return(
        <section id="about" className="about">
                    <div className="whiteBg">
                        <div className="wrapper aboutGrid">
                                <div className="aboutHeader">
                                    <h1><span>dovolen?</span> Моментальная безналичная благодарность</h1>
                                </div>
                                <div className="aboutText">   
                                    
                                    <p>Сервис <a href="/">dovolen</a> дает возможность клиенту оставить отзыв и/или&nbsp;отблагодарить персонал безналично.</p>
                                    
                                      <ul>
                                        <li><ScrollLink to="integration" smooth={true} offset={-40} duration={500} >Где разместить QR-код?</ScrollLink></li>
                                        <li><ScrollLink to="price" smooth={true} offset={-40} duration={500} >Тарифы</ScrollLink></li>
                                      </ul>                                  

                                </div> 
                                <div className="aboutTeaser">
                                    <div className="teaser">
                                        <div className="teaserPrint">
                                          <img src={teaserPrint} alt="" />
                                        </div>
                                        <div className="teaserPhone">
                                            <img src={teaserPhone} alt="" />
                                        </div>
                                    </div>                                    
                                </div>    
                        </div>
                    </div>
                    <div className="whiteSmile">
                      <div className="ct">
                        <Button  variant="contained" color="primary" size="large" className="actionButton">Подключиться</Button>                        
                        <Button  variant="outlined" color="primary" size="large" className="enterButton">Вход</Button>
                      </div>                      
                    </div>                   
                    
        </section>
    );
}

export default About;