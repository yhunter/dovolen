//Компонент отображения страницы Заведения у менеджера со списком работников
import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, IconButton, Button, TextField} from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/HighlightOff';
import PersonAddIcon from '@material-ui/icons/PersonAdd';


const StyledTableCell = withStyles((theme) => ({ //Стилизация таблицы
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {

  },
}))(TableRow);


export default function PlaceWorkers(props) {
  const classes = useStyles();

  let counter=0;

  const emptyWorker = {
            "name": "",
            "tel": "",
            "email": "",
            "position": ""
  }

  const [dialogStatus, setOpen] = React.useState({"open": false, "worker": emptyWorker});

  const handleClickOpen = (title="Добавить сотрудника", worker=emptyWorker) => {
    setOpen({
      "open": true,
      "title": title,
      "worker": worker,
    });
  };

  const handleClose = () => {
    setOpen({"open": false, "worker": emptyWorker, "title": dialogStatus.title});
  };

  return (
    <div>   
        
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell >Список сотрудников</StyledTableCell>
                <StyledTableCell align="center">Телефон</StyledTableCell>
                    <StyledTableCell align="center">Email</StyledTableCell>
                <StyledTableCell align="right" >
                    <IconButton color="secondary" onClick={() => {handleClickOpen("Добавить сотрудника")}} aria-label="add an alarm" title="Добавить сотрудника" className={classes.addWorker}>
                        <PersonAddIcon />
                    </IconButton>
                </StyledTableCell>
                    
                
              </TableRow>
            </TableHead>
            <TableBody>
              {props.workers.map((row) => (
                <StyledTableRow key={row.name+counter++}>

                  <StyledTableCell>
                    <div className={classes.workerCell}>
                        <div className={classes.workerName}>
                          {row.name}<br/>
                          <span>{row.position}</span>

                        </div>                      
                        
                    </div>
                    <div className="shadow"></div>
                    
                    
                  
                  </StyledTableCell>
                  <StyledTableCell  align="center"><a href={"tel:"+row.tel}>{row.tel}</a><div className="shadow"></div></StyledTableCell>
                    <StyledTableCell  align="center"><a href={"tel:"+row.email}>{row.email}</a><div className="shadow"></div></StyledTableCell>

                  <StyledTableCell align="right">
                    <IconButton onClick={() => {handleClickOpen("Редактировать данные сотрудника", row)}} color="secondary" aria-label="add an alarm" title="Редактировать">
                        <EditIcon />
                    </IconButton>
                    <IconButton color="secondary" aria-label="add an alarm"  title="Удалить">
                        <DeleteIcon />
                    </IconButton>
                  </StyledTableCell>

                </StyledTableRow>
              ))}
            </TableBody>
            
            
          </Table>
          <Dialog open={dialogStatus.open} onClose={handleClose} aria-labelledby="form-dialog-title">
              <DialogTitle id="form-dialog-title">{dialogStatus.title}</DialogTitle>
            <DialogContent>
              <TextField
                defaultValue={dialogStatus.worker.name}
                margin="dense"
                id="workerName"
                label="Имя, фамилия"
                type="text"
                fullWidth
              />
              <TextField
                defaultValue={dialogStatus.worker.tel}
                margin="dense"
                id="workerTel"
                label="Телефон"
                type="tel"
                fullWidth
              />
              <TextField
                defaultValue={dialogStatus.worker.email}
                margin="dense"
                id="name"
                label="Email"
                type="email"
                fullWidth
              />
              <TextField 
                defaultValue={dialogStatus.worker.position}               
                margin="dense"
                id="workerPosition"
                label="Должность"
                type="text"
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary" variant="contained">
                Сохранить
              </Button>
              <Button onClick={handleClose} color="primary" >
                Отмена
              </Button>
            </DialogActions>
          </Dialog>
        </TableContainer>
    </div>
  );
}


const useStyles = makeStyles((theme) => ({
  root: {

  },
  preTable: {
    marginBottom: ".5rem",
  },
  table: {
    [theme.breakpoints.down('sm')]: {
      tableLayout: "fixed",
    } ,
    position: "relative",
    overflow: "hidden",
    "& a": {
      color: "#696983",
    },
    "& tr": {
      "& .shadow": {
        display: "block",
        content: "",
        width: "1rem",
        height: "100%",
        position: "absolute",
        backgroundColor: "#fff",
        right: 0,
        top: 0,
        background: "linear-gradient(-90deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%)",

      },
      "& td": {
        overflow: "hidden",
        position: "relative",
        [theme.breakpoints.down('xs')]: {
          fontSize: ".8rem",
        },
      },
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
        "& .shadow": {
          background: "linear-gradient(-90deg, rgba(245,245,245,1) 0%, rgba(245,245,245,0) 100%) !important",
  
        },
      },
    },
    
  },
  workerCell: {
    display: "flex",
    flexDirection: "column",
    [theme.breakpoints.up('md')]: {
      flexDirection: "row",  
    },
    "& div": {
      flex: 1,
      textAlign: "center",
      alignItems: "center",
      justifyContent: "center",
      [theme.breakpoints.down('md')]: {
        textAlign: "left", 
        justifyContent: "flex-start", 
      },
    },
  },
  workerName: {
    margin: "0 1.5rem .25rem 0",
    textAlign: "left !important",
    "& span": {
      opacity: ".5",
    }
  },
  workerTel: {
    display: "flex",
    margin: "0 1.5rem .25rem 0",
    "& a": {
      display: "flex",
      alignItems: "center",
    }
  },
  workerEmail: {
    display: "flex",
    margin: "0 1.5rem .25rem 0",
  },
  tableContainer: {
    margin: '0 1.5rem .25rem 0',
  },
  addWorker: {
    //backgroundColor: "#fff",
    color: "#fff",
    "&:hover": {
      //backgroundColor: "#fff",
    }
  },
  tableHeader: {
    display: "flex",
    "& span": {
      flex: 1,
      height: "100%"
    }
  }

}));