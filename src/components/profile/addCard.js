//Компонент добавления карты с полями для ввода
import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import NumberFormat from 'react-number-format';

import {Paper, Grid, TextField, InputLabel, FormControl, Select, Button} from '@material-ui/core';


  
  function NumberFormatCustom(props) {  //Для маски номера карты
    const { inputRef, onChange, ...other } = props;
  
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          });
        }}
        format="####     ####     ####     ####"
        isNumericString        
      />
    );
  }


export default function AddCard(props) {
    const classes = useStyles();

    const [values, setValues] = React.useState({ //Для маски номера карты
      numberformat: '',
    });
  
    const handleChange2 = (event) => { //Для маски номера карты
      setValues({
        ...values,
        [event.target.name]: event.target.value,
      });
    };
    
    const [state, setState] = React.useState({ //Для срока карты
      month: '',
      year: '',
    });

    const handleChange = (event) => { //Для срока карты
      const name = event.target.name;
      setState({
        ...state,
        [name]: event.target.value,
      });
    };
    
    return (
      <div>
        <Paper className={classes.card} elevation={3}>
            <div className={classes.logo__bg} />              

            <Grid container spacing={1}>

              <Grid item xs={12}>
              
                <TextField
                  label="Номер карты"
                  value={values.numberformat}
                  onChange={handleChange2}
                  name="numberformat"
                  fullWidth
                  variant="outlined"
                  className={classes.cardNoField}
                  id="formatted-numberformat-input"
                  InputProps={{
                    inputComponent: NumberFormatCustom,                    
                  }}
                />
                
              </Grid>
              <Grid item xs={4}  className={classes.card__exp}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="month">Месяц</InputLabel>
                <Select
                  native
                  value={state.month}
                  onChange={handleChange}
                  inputProps={{
                    name: 'month',
                    id: 'month',
                  }}
                >
                  <option aria-label="None" value="" />
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                  <option value={4}>4</option>
                  <option value={5}>5</option>
                  <option value={6}>6</option>
                  <option value={7}>7</option>
                  <option value={8}>8</option>
                  <option value={9}>9</option>
                  <option value={10}>10</option>
                  <option value={11}>11</option>
                  <option value={12}>12</option>
                </Select>
              </FormControl>
              </Grid>
              <Grid item xs={4}  className={classes.card__exp}>
              <FormControl className={classes.formControl}>
                <InputLabel htmlFor="year">Год</InputLabel>
                <Select
                  native
                  value={state.year}
                  onChange={handleChange}
                  inputProps={{
                    name: 'year',
                    id: 'year',
                  }}
                >
                  <option aria-label="None" value="" />
                  <option value={20}>20</option>
                  <option value={21}>21</option>
                  <option value={22}>22</option>
                  <option value={23}>23</option>
                  <option value={24}>24</option>
                  <option value={25}>25</option>
                  <option value={26}>26</option>
                  <option value={27}>27</option>
                  <option value={28}>28</option>
                  <option value={29}>29</option>
                  <option value={30}>30</option>
                  <option value={31}>31</option>
                  <option value={32}>32</option>
                </Select>
              </FormControl>
              </Grid>
              <Grid item xs={4}>
                <TextField id="CVV" type="text" label="CVV" color="secondary" variant="outlined" fullWidth />
              </Grid> 
              <Grid item xs={12}>
                <TextField id="HolderName" type="text" label="Имя владельца" color="secondary" variant="outlined" fullWidth  />
              </Grid>
            </Grid>          
            
        </Paper>
        <div className={classes.walletButtons}>
            <TextField id="CardName" type="text" label="Наименование карты" color="secondary"  fullWidth />   <br />   
            <Button variant="contained" color="primary">
                Сохранить карту
            </Button>
            <Button variant="outlined" color="primary" disabled>
                Удалить карту
            </Button>
        </div>
    </div>
    );    
}

const useStyles = makeStyles((theme) => ({
  card: {
    borderRadius: '1rem',
    width: '19rem',
    fontSize: '.7rem', 
    height: 'calc(19rem/1.4)',   
    display: 'flex',
    position: 'relative',
    overflow: 'hidden',
    border: "1px #ccc solid",
    color: "#000",  
    margin: "0 auto 0rem auto",
    padding: "1rem",
    alignItems: "center",
    [theme.breakpoints.up('sm')]: {
      width: '40vw',
      height: 'calc(40vw/1.4)',
      margin: "0 auto 0rem auto",
      fontSize: "1.2vw",
    },
    [theme.breakpoints.up('lg')]: {
      width: '24vw',
      height: 'calc(24vw/1.6)',
      margin: "0 auto 0rem 0",
      fontSize: ".85vw",
    }, 
  },

  visa__logo: {
    width: "70%",
    display: "block",
    margin: "0 0 0 auto",
  },
  card__no: {
    letterSpacing: "0.40em",
    fontSize: "1.58em",
    textAlign: "center",
    whiteSpace: "nowrap",
    textShadow: "2px 2px 2px rgba(30,30,30, .4)",
    margin: "1em 0",
    transform: "translateX(.15em)",
  },
  card__exp: {
    fontSize: "1.5em",
    display: "flex",
    alignItems: "center",
  },
  formControl: {
    width: "100%",
  },
  cardNoField: {
    "& input": {
      
      textAlign: "center"
    }
  },
  walletButtons: {
      margin: "0rem auto 2.5rem auto",
      textAlign: "center",
      "& button": {
        
        marginRight: ".5rem", 
        marginTop: ".5rem",         
      },
  },
}));


