//Компонент отображения таблицы чаевых клиента
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';

import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';

import Paper from '@material-ui/core/Paper';

import IconButton from '@material-ui/core/IconButton';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Rating from '@material-ui/lab/Rating';


import ResponseIcon from '@material-ui/icons/Announcement';

//Функции для таблицы с сортировкой (пример material ui)
function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  { id: 'name', numeric: false, disablePadding: false, label: 'Сотрудник' },
  { id: 'rating', numeric: true, disablePadding: false, label: 'Оценка' },
  { id: 'tips', numeric: true, disablePadding: false, label: 'Чаевые' },
  { id: 'response', numeric: true, disablePadding: false, label: 'Отзыв' },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy,  onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  let counter = 0;

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id+counter++}
            align={headCell.numeric ? 'center' : 'left'}            
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}

      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};









export default function AnalyticsManager(props) {

  const rows = props.data;

  const classes = useStyles();
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  //Модальное окно отзыва
  const [dialogStatus, setOpen] = React.useState({"open": false, "response": "", "author": ""});

  const handleClickOpen = (response, author) => { //Передаем сюда текст отзыва и отображаем в модальном окне
    setOpen({
      "open": true,
      "response": response,
      "author": author,
    });
  };

  const handleClose = () => {
    setOpen({"open": false, "response": dialogStatus.response, "author": dialogStatus.author});
  };


  let counter = 0; //Для ключей элементов таблицы

  

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        
        <TableContainer style={{"maxWidth": "100%"}}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                
                .map((row, index) => {

                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      
                      onClick={(event) => handleClick(event, row.name)}


                      tabIndex={-1}
                      key={row.name+counter++}

                    >

                      <TableCell component="th" id={labelId} scope="row" className="workerName" >
                        {row.name} 
                        <div className={classes.date}>
                          {row.date}
                        </div>
                        <div className="shadow"></div>
                      </TableCell>
                      <TableCell align="center" className={classes.rating}>
                          
                          <Rating                                 
                                value={row.rating} 
                                readOnly 
                                size="small"
                          />
                      </TableCell>
                      <TableCell align="center">{row.tips}&nbsp;₽</TableCell>
                      <TableCell align="center" className={classes.response}>
                        
                        <IconButton color="secondary"  title="Показать отзыв" onClick={()=>{handleClickOpen(row.response, row.author)}} className={(row.response.length>0) ? "showResponse" : "hideResponse" }>
                          <ResponseIcon  />
                        </IconButton>
                      </TableCell>
                      
                    </TableRow>
                  );
                })}

            </TableBody>
          </Table>
        </TableContainer>

      </Paper>
      <Dialog open={dialogStatus.open} onClose={handleClose} aria-labelledby="form-dialog-title">
              <DialogTitle id="form-dialog-title">Отзыв клиента</DialogTitle>
              <DialogContent>
                {dialogStatus.response}
                <div style={{"opacity": .5, "marginTop": ".5rem"}}>{dialogStatus.author}</div>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} color="primary" variant="contained">
                  Закрыть
                </Button>
              </DialogActions>
      </Dialog>
    </div>
  );
}


const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  response: {

    "& .hideResponse": {
        display: "none",
    },
    "& .showResponse": {
        
    },
  },
  date: {
    opacity: ".5"
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
    overflow: "hidden",
  },

  table: {
    minWidth: 0,
    position: "relative",
    [theme.breakpoints.down('sm')]: {
      tableLayout: "fixed",
    },  
    "& thead": {
        "& svg": {
          position: "absolute",
          right: "-1.5rem",
          [theme.breakpoints.down('xs')]: {
            display: "none !important",
          }
        },
        backgroundColor: theme.palette.secondary.main,
        color: "#fff !important",
        "& .MuiTableCell-head, & .MuiTableSortLabel-icon": {
            color: "#fff !important",
        },
        "& .MuiTableSortLabel-active": {
            color: "#fff",
            fontWeight: "bold"
        },
        "& .MuiTableSortLabel-root:hover, & .MuiTableSortLabel-root:active": {
            color: "rgba(255,255,255, .6)",
        }
    },
    "& tr": {
        
        "& .workerName": {
          position: "relative",
          overflow: "hidden",
          "& .shadow": {
            display: "block",
            content: "",
            width: "2rem",
            height: "100%",
            position: "absolute",
            backgroundColor: "#fff",
            right: 0,
            top: 0,
            background: "linear-gradient(-90deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%)",
          },
        },
        '&:nth-of-type(odd)': {
          backgroundColor: theme.palette.action.hover,
          "& .shadow": {
            background: "linear-gradient(-90deg, rgba(245,245,245,1) 0%, rgba(245,245,245,0) 100%) !important",
          }
      },
    },

  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  rating: {
    [theme.breakpoints.down('xs')]: {
      transform: "scale(.8)",
    }
  },
  
}));