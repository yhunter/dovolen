//Компонент вкладок на главной странице личного кабинета с QR-кодом и ссылкой другу
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import {AppBar, Tab, TextField, Button} from '@material-ui/core';

//Табы берем из ветки lab, чтобы содержимое отображалось в блоках, а не в параграфах для валидной верстки
import {TabContext, TabList, TabPanel} from '@material-ui/lab';

import qr from'../../assets/qr-code.gif';

  
export default function SimpleTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState('1'); //Стейт для табов 

    const handleChange = (event, newValue) => { //переключение вкладок
      setValue(newValue);
    };
  
    return (
      <div className={classes.tabPanel}>
        <TabContext value={value}>
          <AppBar position="static" className={classes.tabs} elevation={0}>
            <TabList onChange={handleChange} aria-label="simple tabs example" classes={{indicator: classes.indicator}}>
              <Tab label="Ваш QR-код" value="1" />
              <Tab label="Пригласить друга" value="2" />
            </TabList>
          </AppBar>
          <TabPanel value="1" index="1">
              <h3>Ваш код для получения чаевых</h3>              
              <img  src={qr} alt="Ваш qr-код"/>
              <b>000123</b>
              <div className={classes.walletButtons}>
                <Button variant="contained" color="primary">Добавить в GPay</Button>&nbsp;
                <Button variant="contained" color="primary">Добавить в Apple Wallet</Button>
              </div>
              <form className={classes.root} noValidate autoComplete="off">
                <TextField id="UserLink" label="Ссылка на страницу оплаты" variant="outlined" fullWidth value="https://dovolen.me/pay000123" />
              </form> 
              <Link  to="/" className={classes.links} >Заказать карту получения чаевых</Link>
          </TabPanel>
          <TabPanel value="2" index="2">
              <h3>Пригласить друга</h3>
              <p>Вы можете пригласить друзей в сервис и дать им возможность просто и легко получать чаевые на карту.</p>
              <form className={classes.root} noValidate autoComplete="off">
                <TextField id="PartnerLink" label="Пригласить через email" variant="outlined" fullWidth />
                <div className={classes.walletButtons}>
                  <Button variant="contained" color="primary">
                    Отправить другу
                  </Button>
                </div>
              </form>              
              <TextField id="filled-basic" label="Ссылка для приглашения" variant="outlined" fullWidth value="https://dovolen.me/invite000123" />
          </TabPanel>
        </TabContext>
      </div>
    );
} 

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    "& a:visited": {
      color: "#f33",
    }
  },
  
  tabs: {
      backgroundColor: "#fff",
      color: "#111",
      "& button": {
        marginTop: "0 !important",
      }
  },
  indicator: {
      backgroundColor: "#f47522",
  },
  h3: {
      textAlign: "center",
  },
  links: {
    color: theme.palette.primary.main,
  },
  walletButtons: {
    margin: ".5rem 0 2rem 0",
    "& button": {
      marginRight: ".5rem",          
    },
  },
  tabPanel: {
      textAlign: "center",
      '& h3': {            
          marginBottom: "0",
      },
      '& img': {
          margin: "1rem auto",
          display: "block",
      },
      '& p': {            
        textAlign: "left",
      },
      '& b': {            
        textAlign: "center",
        display: "block",
        fontSize: "1.75rem",
        
      },
      "& button": {
        margin: ".5rem .25rem 0rem .25rem",
      },
      "& form": {
        marginBottom: "1.5rem",
      }
  },
  
}));
  