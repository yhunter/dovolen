//Компонент отображения последних событий баланса
import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper} from '@material-ui/core';

const StyledTableCell = withStyles((theme) => ({ //Стилизация таблицы
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(name, change, placeName, tips) { //Функция заполнения таблицы данными из демки
  return { name, change, placeName, tips};
}

const rows = [
  createData('Чаевые', 10, '«Art-Caviar»', 1),
  //createData('Комиссия сервиса', -1), //Правки: Убрано отображение уменьшения баланса
  createData('Чаевые', 50, '«Art-Caviar»', 5),
  //createData('Комиссия сервиса', -5),
  createData('Чаевые', 300, '«Art-Caviar»', 30),
  //createData('Комиссия сервиса', -15),
];

const useStyles = makeStyles({
  table: {

  },
  tableContainer: {
    marginBottom: '1.5rem',
  },
  positive: {
      color: "#59aa76",
  }
});

const PositiveChange = (change) => { //Если изменение баланса положительное, оборачиваем текст в жирный зеленый
    const classes = useStyles();
    if (change>0) {
        return (
            <b className={classes.positive}>
                {change}
            </b>
        )
    }
    else {
        return (
            <>
                {change}
            </>
        )
    }
}



export default function LastEvents(props) {
  const classes = useStyles();

  let counter=0;

  const BuildHistoryEvents = (row, role="recepient") => {//Проверяем роль пользователя. Если агент, то выводим название заведения и комиссию агента
    if (role==="agent") {
      return(
        <>
                  <StyledTableCell component="th" scope="row">
                    {row.placeName}
                  </StyledTableCell>
                  <StyledTableCell align="right">
                    {row.change}
                  </StyledTableCell>
                  <StyledTableCell align="right">
                    {PositiveChange(row.tips)}
                  </StyledTableCell>
    
        </>
      );
    }
    else {
      return (
        <>
                  <StyledTableCell component="th" scope="row">
                    {row.name}
                  </StyledTableCell>
                  <StyledTableCell align="right">{PositiveChange(row.change)}</StyledTableCell>
    
        </>
      );
    }
  }

  const BuildEventsHeader = (date, role="recepient") => {
    if (role==="agent") {
      return(
        <TableRow>
              <StyledTableCell>24 сентября</StyledTableCell>
              <StyledTableCell align="right">Чаевые</StyledTableCell>
              <StyledTableCell align="right">%</StyledTableCell>
        </TableRow>
      );
    }
    return(
      <TableRow>
            <StyledTableCell  colSpan={2}>24 сентября</StyledTableCell>
      </TableRow>
    );
  }
  
  return (
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          {BuildEventsHeader("24 сентября", props.role)}
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>
              {BuildHistoryEvents(row, props.role)}
            </StyledTableRow>
          ))}
        </TableBody>
        <TableHead>
            {BuildEventsHeader("23 сентября", props.role)}
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>
              {BuildHistoryEvents(row, props.role)}
            </StyledTableRow>
          ))}
        </TableBody>
        <TableHead>
            {BuildEventsHeader("18 сентября", props.role)}
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>
                {BuildHistoryEvents(row, props.role)}
            </StyledTableRow>
          ))}
        </TableBody>
        
      </Table>
    </TableContainer>
  );
}
