//Компонент полей добавления нового заведения в настройках получателя и в заведениях агента
import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import {TextField, Select, MenuItem, FormControl, InputLabel, Button, FormControlLabel, Switch} from '@material-ui/core';

function AddPlace(props) {
    return(
        <div>
            <TextField id="NewPlaceName" type="text" label="Название заведения" color="primary" variant="outlined" fullWidth  required={props.required}  />
            <FormControl fullWidth variant="outlined"  required={props.required} >
                <InputLabel id="NewPlaceTypeLabel">Тип заведения</InputLabel>
                <Select
                    labelId="NewPlaceTypeLabel"
                    id="NewPlaceType"
                    fullWidth
                    
                    label="Тип заведения"
                    defaultValue=""
                    //value={age}
                    //onChange={handleChange}
                >
                    <MenuItem value="restaurant">Ресторан</MenuItem>
                    <MenuItem value="cafe">Кафе</MenuItem>
                    <MenuItem value="hotel">Отель</MenuItem>
                    <MenuItem value="courier">Курьерская служба</MenuItem>
                </Select>
            </FormControl>
           
            
            <TextField id="NewPlaceCity" type="text" label="Город" color="primary" variant="outlined" fullWidth required={props.required} />
            <TextField id="NewPlaceAddress" type="text" label="Улица, дом, строение, офис" color="primary" variant="outlined" fullWidth  required={props.required}  />
            <TextField id="NewPlaceUrl" type="url" label="Сайт" color="primary" variant="outlined" fullWidth />
            <TextField id="NewPlaceTel" type="url" label="Телефон" color="primary" variant="outlined" fullWidth />
            <TextField id="NewPlaceEmail" type="email" label="Email" color="primary" variant="outlined" fullWidth />

        </div>
        
    );
}

export default AddPlace;