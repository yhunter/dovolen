import React from 'react';
import { Link } from 'react-router-dom';

import {IconButton, MenuItem, Menu, Avatar, Typography, Hidden} from '@material-ui/core';

import ava from '../../assets/courtney-cook-le7D9QFiPr8-unsplash.jpg'; //Аватарка

export default function User(props) {

  //Состояние и функции для открытия/закрытия меню пользователя
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
            <div> 
                <Hidden only="xs">
                  <Typography variant="h6" style={{
                    "display": "inline-block",
                    "marginRight": ".5rem",
                    "fontWeight": "bold",
                    "transform": "translateY(.25rem)",
                  }}>
                    {props.user}
                  </Typography>
                </Hidden>                                        
                <IconButton onClick={handleMenu} size="small">
                  <Avatar alt="Василиса Константинопольская" src={ava}  aria-label="account of current user" aria-controls="menu-appbar" aria-haspopup="true"   /> 
                </IconButton>
                
                <Menu
                  id="menu-appbar"
                  anchorEl={anchorEl}
                  anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  keepMounted
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                  }}
                  open={open}
                  onClose={handleClose}
                >
                  <MenuItem component={Link} to={"/"+props.role+"/settings"}>Мои настройки</MenuItem>
                  <MenuItem onClick={handleClose}>Выход</MenuItem>
                </Menu>
            </div>

  );
}
