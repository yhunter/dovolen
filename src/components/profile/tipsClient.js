//Компонент отображения последних событий баланса
import React from 'react';
import { withStyles, makeStyles} from '@material-ui/core/styles';
import {Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Button} from '@material-ui/core';

import RepeatIcon from '@material-ui/icons/Cached';
import CreateIcon from '@material-ui/icons/Create';



const StyledTableCell = withStyles((theme) => ({ //Стилизация таблицы
  head: {
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.common.white,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      //backgroundColor: theme.palette.action.hover,
      "& .shadow": {
      //      background: "linear-gradient(-90deg, rgba(245,245,245,1) 0%, rgba(245,245,245,0) 100%) !important",
      }
    },
  },
}))(TableRow);

function createData(date, place, name, change) { //Функция заполнения таблицы данными из демки
  return { date, place, name, change};
}

const rows = [
  createData('23 сентября', 'Ресторан «Сапиенс эст»', 'Василиса Константинопольская', 500),
  //createData('Комиссия сервиса', -1), //Правки: Убрано отображение уменьшения баланса
  createData('20 сентября', 'Отель «Аврора Парк»', 'Василиса Константинопольская', 300),
  //createData('Комиссия сервиса', -5),
  createData('4 сентября', 'Доставка «Delivery Club»', 'Василиса Константинопольская', 100),
  //createData('Комиссия сервиса', -15),
];

export default function TipsClient() {
  const classes = useStyles();


  let counter=0;

  return (
    <TableContainer component={Paper} className={classes.tableContainer}>
      <Table className={classes.table} aria-label="customized dense table" dense="small" >
        <TableHead>
          <TableRow>
            <StyledTableCell colSpan="2">
                23 сентября            
            </StyledTableCell>         

          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>

              <StyledTableCell component="th" scope="row" className="nameCell">
                <div className="place">
                    {row.place}
                </div>
                <div className="name">
                    {row.name}
                </div>                     
                <div className="tableButtons">
                    <Button  color="primary" size="small" startIcon={<RepeatIcon />}>Повторить чаевые</Button>
                    <Button  color="secondary" size="small" startIcon={<CreateIcon />}>Написать сотруднику</Button>
                </div>
                <div className="shadow"></div>             
                

              </StyledTableCell>

              <StyledTableCell align="right" component="th" scope="row">
                {row.change+"\u00A0₽"}
              </StyledTableCell>


            </StyledTableRow>
          ))}
        </TableBody>
        <TableHead>
          <TableRow>
            <StyledTableCell colSpan="2">
                19 сентября            
            </StyledTableCell>         

          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>

              <StyledTableCell component="th" scope="row" className="nameCell">
                <div className="place">
                    {row.place}
                </div>
                <div className="name">
                    {row.name}
                </div>                     
                <div className="tableButtons">
                    <Button color="primary" size="small" startIcon={<RepeatIcon />}>Повторить чаевые</Button>
                    <Button color="secondary" size="small" startIcon={<CreateIcon />}>Написать сотруднику</Button>
                </div>
                <div className="shadow"></div>             
                

              </StyledTableCell>

              <StyledTableCell align="right" component="th" scope="row">
                {row.change+"\u00A0₽"}
              </StyledTableCell>


            </StyledTableRow>
          ))}
        </TableBody>
        <TableHead>
          <TableRow>
            <StyledTableCell colSpan="2">
                5 сентября            
            </StyledTableCell>         

          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <StyledTableRow key={row.name+counter++}>

              <StyledTableCell component="th" scope="row" className="nameCell">
                <div className="place">
                    {row.place}
                </div>
                <div className="name">
                    {row.name}
                </div>                     
                <div className="tableButtons">
                    <Button  color="primary" size="small" startIcon={<RepeatIcon />}>Повторить чаевые</Button>
                    <Button  color="secondary" size="small" startIcon={<CreateIcon />}>Написать сотруднику</Button>
                </div>
                <div className="shadow"></div>             
                

              </StyledTableCell>

              <StyledTableCell align="right" component="th" scope="row">
                {row.change+"\u00A0₽"}
              </StyledTableCell>


            </StyledTableRow>
          ))}
        </TableBody>
        
      </Table>
    </TableContainer>
  );
}

const useStyles = makeStyles((theme) => ({
  table: {
    maxWidth: '100%',
    "& .lgUp": {
        display: "none",
        [theme.breakpoints.up('lg')]: {
            display: "table-cell",
        }
    },
    "& .place": {
        fontWeight: "bold",
        fontSize: "1rem",
        [theme.breakpoints.up('sm')]: {
            fontSize: "1.25rem"  
        },
    },
    "& .nameCell": {
        display: "flex",
        flexDirection: "column",
        position: "relative",
        "& .shadow": {
            display: "block",
            content: "",
            width: "2rem",
            height: "100%",
            position: "absolute",
            backgroundColor: "#fff",
            right: 0,
            top: 0,
            background: "linear-gradient(-90deg, rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%)",
        }
    },
    "& .tableButtons": {
        margin: ".5rem 0",
        "& button": {
            margin: "0 .5rem .5rem 0"
        }
        
    }
  },
  tableContainer: {
    marginBottom: '1.5rem',
  },
  positive: {
      color: "#59aa76",
  }
}));