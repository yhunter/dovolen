//Компонент контактов агента (для менеджера и сотрудника) в разделах Контакты и Поддержка

import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import {Grid, Button, Typography} from '@material-ui/core';

import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';

function AgentContacts(props) {
    const classes = useStyles();
    if ((props.role==='manager') ?? (props.role==='recepient')) {
        return(
            <div>
                <div className={classes.agentContacts}>
                    <h2>По вопросам организации и работы сервиса обращайтесь к агенту:</h2>
                    <Typography variant="h5" m="3">
                        <PhoneIphoneIcon fontSize="small" /> <a href="tel:+79991234567" className={classes.links}  target="_blank" rel="noopener noreferrer"> +7-999-123-45-67</a> 
                    </Typography>
                    <Typography variant="h5">
                        <AlternateEmailIcon fontSize="small"  /> <a href="mailto:mailbox@dovolen.me" className={classes.links}  target="_blank" rel="noopener noreferrer"> mailbox@dovolen.me</a>
                    </Typography>
                </div>
                <h2>Сервис «dovolen»</h2>
            </div>
        );
    }
    else {
        return(<></>);
    }
}

export default AgentContacts;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      "& a:visited": {
        color: "#f33",
      }
    },

    links: {
      color: theme.palette.secondary.main,
      
      
    },  
    agentContacts: {
        marginBottom: "3rem",
        "& h5": {
            marginBottom: ".25rem",
            padding: "0 0 0 0",
        },

        "& a": {
            margin: "0 .5rem .5rem 0",
        },
        "& p": {
            marginBottom: "1.5rem",
        },
        "& svg": {
            verticalAlign: "middle",
            fill: "rgba(0, 0, 0, 0.54)",
        }
    }  
    
  }));