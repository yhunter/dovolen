import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import {TextField, Select, MenuItem, FormControl, InputLabel, Button, FormControlLabel, Switch} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import AddPlace from './addPlaceFields'; //Поля добавления нового заведения



function SettingsPlace(props) {
    const classes = useStyles();
    const [PlaceState, setPlaceState] = React.useState({ //Стейт переключателя Заведения
        checkedC: false,
        withoutPlace: false,
        addPlaceClass: "",
    });

    const SwitchChange = (event) => { //Переключатель Заведения
        setPlaceState({
            [event.target.name]: event.target.checked,
            withoutPlace: event.target.checked,
        });
        setAddPlaceClass({ //Сворачиваем форму добавления заведения, если она открыта
            show: false, 
            addPlaceClass: ""
        });
    }; 

    const [addPlace, setAddPlaceClass] = React.useState({ //Стейт формы добавления заведения
        show: false, 
        addPlaceClass: ""        
    });

    const ShowNewPlaceForm = () => { //Показываем форму нового заведения (добавляем класс show)       
        let addClass="";
        if (!addPlace.show) {
            addClass="show";
        }
        else {
            addClass="";
        }
        setAddPlaceClass({
            show: addPlace.show ^= true,
            addPlaceClass: addClass
        });
    }; 

    return(
        <form className={classes.contactFields} noValidate autoComplete="off">
                                <FormControlLabel
                                    style={{"marginBottom": ".5rem",}}
                                    control={
                                    <Switch
                                        checked={PlaceState.checkedC}
                                        onChange={SwitchChange}
                                        name="checkedC"
                                        color="primary"
                                    />
                                    }
                                    label="Без заведения"
                                />
                                {/* Если открыта форма добавления заведения стоит "без заведения", отключаем выбор из списка */}
                                <Autocomplete 
                                    id="placeName"
                                    options={placeList}
                                    fullWidth
                                    disabled={Boolean(addPlace.show) || PlaceState.withoutPlace} 
                                    getOptionLabel={(option) => option.title}
                                    
                                    renderInput={(params) => <TextField {...params} label="Выбрать заведение" variant="outlined" fullWidth />}
                                />
                                <div className={classes.walletButtons}>
                                        {/* Кнопка отключена, если стоит "без заведения"  */}
                                        <Button variant="outlined" color="primary" onClick={ShowNewPlaceForm} disabled={(PlaceState.withoutPlace)}>
                                            Добавить новое заведение
                                        </Button>
                                </div>
                                <div className={"hideAddPlace "+addPlace.addPlaceClass}>
                                    <AddPlace required={Boolean(addPlace.show)}  /> {/* Если форма добавления открыта, делаем нужные инпуты обязательными, иначе необязательные */}
                                </div>    
                                
                                {/* Поле отключено, если стоит "без заведения"  */}    
                                <TextField id="Position" type="text" label="Должность" color="primary" variant="outlined" fullWidth disabled={PlaceState.withoutPlace} /> 
                                
                                <div className={classes.walletButtons}>
                                        <Button variant="contained" color="primary">
                                            Сохранить
                                        </Button>
                                </div>
        </form> 
    );
}

export default SettingsPlace;

const placeList = [
    { title: '«Сапиенс эст», г. Москва'},
    { title: '«Пятый океан», г. Владивосток'},
    { title: '«Art-Caviar», г. Санкт-Петербург'},
    { title: '«Дача», г. Тамбов'},
    { title: '«Библиотека», г. Новосибирск'}
];



const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,

    },
    
    
    walletButtons: {
      textAlign: "center",
      margin: ".5rem 0 .5rem 0",
      "& button": {
          marginRight: ".5rem",
          marginBottom: ".5rem",         

      }
    },
    tabPanel: {
        textAlign: "center",
        '& h3': {            
            marginBottom: "0",
        },
        '& img': {
            margin: "1rem auto",
            display: "block",
        },
        '& p': {            
          textAlign: "left",
        },
        '& b': {            
          textAlign: "center",
          display: "block",
          
        },
        "& button": {
          margin: ".5rem .25rem 0rem .25rem",
        },
        "& form": {
          marginBottom: "1.5rem",
        },

    },
    contactFields: {
        " & .MuiOutlinedInput-root": {
            marginBottom: "1rem",
        },
        "& .hideAddPlace": {
            padding: ".5rem 0 0 0",            
            position: "relative",
            display: "block",
            maxHeight: "0px",
            overflow: "hidden",
            transition: "all .5s ease-in-out"
        },
        "& .show": {
            maxHeight: "1000px"
        },
    }
    
  }));