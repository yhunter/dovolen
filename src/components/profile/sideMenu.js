//Компонент отображения бокового меню. Данные передаются из constants.js
import React from 'react';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

import {List, ListItem, ListItemIcon, ListItemText, Divider} from '@material-ui/core';

import logo from'../../assets/logo.svg'; //Логотип Dovolen

function SideMenu(props) {
    const classes = useStyles();    
    
    return(
      <div>
          <div className={classes.toolbar}>
          <a href="/temp/dovolen/" title="На главную страницу">
            <img  src={logo} alt="dovolen!" className={classes.logo} />
          </a>
      </div>
      
      <List  className={classes.menu}> {/* Верхняя часть меню */}
        {props.list[props.role].menu1.map((item, index) => (
          <ListItem button key={item} selected={props.selected===item} onClick={props.handle} component={Link} to={"/"+props.role+"/"+item}>
            <ListItemIcon>{props.list[item].icon}</ListItemIcon>
            <ListItemText primary={props.list[item].name} />
          </ListItem>
        ))}
      </List>      

      <Divider />
      <List className={classes.menu}> {/* Нижняя часть меню */}
          {props.list[props.role].menu2.map((item, index) => (
            <ListItem button key={item} selected={props.selected===item} component={Link} to={"/"+props.role+"/"+item}>
              <ListItemIcon>{props.list[item].icon}</ListItemIcon>
              <ListItemText primary={props.list[item].name} />
            </ListItem>
          ))}
      </List>
        </div>
    );   
}

export default SideMenu;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',    
  },

  logo: {
    margin: "1rem",
  },

  menu: {
    "& .Mui-selected": { 
      backgroundColor: "#696983",
      color: "#fff",
      "& svg": {
        color: "#fff",
      }, 
      "&:hover": {
        backgroundColor: "#7287a9",
      }
    },
  },


}));