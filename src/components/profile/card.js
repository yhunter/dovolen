//Компонент отрисовки банковской карты
import React from 'react';
import { makeStyles} from '@material-ui/core/styles';

import {Typography, Grid, Paper} from '@material-ui/core';

import logoBg from'../../assets/logoBg.svg'; //Изображение для фона карты
import visa from'../../assets/visa.svg'; //Логотип Visa



export default function Card(props) {
    const classes = useStyles();
    
    return (
        <Paper className={classes.card} elevation={3}>
            <div className={classes.logo__bg} />            

            <Grid container spacing={0}>
              <Grid item xs={12}>                
                <Typography variant="h4">
                  1916.95 ₽
                </Typography>
                <Typography variant="subtitle1">
                  Ваш баланс
                </Typography>  
              </Grid>
              <Grid item xs={12}>
                <div className={classes.card__no}>4783 •••• •••• 6327</div>
              </Grid>
              <Grid item xs={4}  className={classes.card__exp}>
                ••/••
              </Grid>
              <Grid item xs={4}  className={classes.card__exp}>
                cvv •••
              </Grid>
              <Grid item xs={4}>
                <img src={visa} className={classes.visa__logo} alt="" />
              </Grid>
            </Grid>
        </Paper>
    );    
}

const useStyles = makeStyles((theme) => ({
  card: {
    borderRadius: '1rem',
    width: '19rem',
    fontSize: '.7rem', 
    height: 'calc(19rem/1.6)',   
    display: 'flex',
    position: 'relative',
    overflow: 'hidden',
    background: "linear-gradient(-45deg, #f23825 0%,#f47522 100%)",
    color: "#fff",  
    margin: "0 auto 1.5rem auto",
    padding: "1rem",
    alignItems: "center",
    [theme.breakpoints.up('sm')]: {
      width: '40vw',
      height: 'calc(40vw/1.6)',
      margin: "0 auto 1.5rem 0",
      fontSize: "1.2vw",
    },
    [theme.breakpoints.up('lg')]: {
      width: '24vw',
      height: 'calc(24vw/1.6)',
      margin: "0 auto 1.5rem 0",
      fontSize: ".85vw",
    }, 
  },
  logo__bg: {
    backgroundImage: `url(${logoBg})`,
    backgroundSize: "contain",
    backgroundPosition: "100%, 100%",
    backgroundRepeat: 'no-repeat',
    top: "-10%",
    opacity: .1,
    width: "100%",
    height: "190%",
    position: "absolute", 
  },
  visa__logo: {
    width: "70%",
    display: "block",
    margin: "0 0 0 auto",
  },
  card__no: {
    letterSpacing: "0.40em",
    fontSize: "1.58em",
    textAlign: "center",
    whiteSpace: "nowrap",
    textShadow: "2px 2px 2px rgba(30,30,30, .4)",
    margin: "1em 0",
    transform: "translateX(.15em)",
  },
  card__exp: {
    fontSize: "1.5em",
    display: "flex",
    alignItems: "center",
  }
}));