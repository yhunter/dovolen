//Компонент страницы профиля для получателей чаевых
import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import {Paper, Grid, Button} from '@material-ui/core';

import Balance from './balance.js'; // Баланс и изображение карточки
import TabsField from './tabsField.js'; // Табы с QR и ссылкой на партнерку 
import LastEvents from './lastEvents.js'; // Таблица последних изменений баланса 



function MainRecepient(props) {
    const classes = useStyles();
    return (
        
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={8} xl={8}>
                <Balance role={props.role} />  {/* Баланс и изображение карточки */}               
                <h2>Последние события:</h2>
                <LastEvents role={props.role}  /> {/* Таблица последних изменений баланса */}
                <div className={classes.centerText}>
                <Button variant="contained" color="primary" to={"/"+props.role+"/history"} component={Link}>
                    История баланса
                </Button>
                </div>
                
            </Grid>
            <Grid item xs={12} md={12} lg={4} xl={4}>
                <Paper className={classes.paper} elevation={1}> {/* Табы с QR и ссылкой на партнерку */}
                    <TabsField />
                </Paper>
            </Grid>
        </Grid>     
        
      
    );
}

export default MainRecepient;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',    
  },
  a: {
    color: theme.palette.primary.main,
  },    
  
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  paper: {
    borderRadius: ".5rem",
  },
  centerText: {
    textAlign: "center", 
  }

}));