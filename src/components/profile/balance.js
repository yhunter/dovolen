//Компонент баланса с изображением карты
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import {Typography, Grid, Button} from '@material-ui/core/';

import Card from './card.js'; //Компонент отображения карты

const useStyles = makeStyles((theme) => ({
    balanceText: {
        color: theme.palette.secondary.main, 
        [theme.breakpoints.down('xs')]: {
            
            textAlign: "center"
        }       
    }
  }));

export default function Balance(props) {
    const classes = useStyles();
   
    return (
        <Grid container spacing={0}>
          <Grid item xs={12} lg={6}>
            <Card />            
          </Grid>
          <Grid item xs={12} lg={6}>
            <Grid container spacing={2} className={classes.balanceText}>
                <Grid item xs={12}>
                    <Typography variant="h5">
                        Баланс за неделю:
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <Typography variant="h5">
                        660&nbsp;₽ 
                    </Typography> 
                    <Typography variant="subtitle2">
                        Зачислено чаевых
                    </Typography>   
                </Grid>
                <Grid item xs={6}>
                    <Typography variant="h5">
                        285&nbsp;₽ 
                    </Typography>
                    <Typography variant="subtitle2">
                        Выплаты на&nbsp;карту
                    </Typography>    
                </Grid>
                <Grid item xs={6}>
                    <Typography variant="h5">
                        1916.95&nbsp;₽ 
                    </Typography>
                    <Typography variant="subtitle2">
                        Доступно к&nbsp;выводу
                    </Typography>    
                </Grid> 
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" to={"/"+props.role+"/withdraw"} component={Link}>
                        Вывести на карту
                    </Button>    
                </Grid>
            </Grid>    
                       
          </Grid>
        </Grid>        
    );    
}