import React from 'react';

import { makeStyles} from '@material-ui/core/styles';

import { AppBar, Typography, Tab, TextField, Button, FormControlLabel, Switch} from '@material-ui/core';

//Табы берем из ветки lab, чтобы содержимое отображалось в блоках, а не в параграфах для валидной верстки
import {TabContext, TabList, TabPanel} from '@material-ui/lab';

import SettingsPlace from './settingsPlace';


function SettingsTab(props) {
    const classes = useStyles();
    const [value, setValue] = React.useState('1'); //Стейт табов

    const handleChange = (event, newValue) => { //Переключение табов
      setValue(newValue);
    };

    const [state, setState] = React.useState({ //Стейт переключателя СМС
        checkedB: true,
    });
       
    const SwitchChange = (event) => { //Переключатель СМС
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    const role = props.role;

    function sms() {
        return(
            <TabPanel value="3" index="3"> {/* Вкладка СМС*/}  
                                <FormControlLabel
                                    control={
                                    <Switch
                                        checked={state.checkedB}
                                        onChange={SwitchChange}
                                        name="checkedB"
                                        color="primary"
                                    />
                                    }
                                    label="Присылать оповещения по SMS"
                                />
                                <p>Услуга отправки оповещений по SMS ― платная. 3 ₽/сообщение.</p>
    
                                <p>Для получения бесплатных Пуш-уведомлений о состоянии баланса, добавьте ваш счет в Google Pay.</p>
                                <div className={classes.walletButtons}>
                                    <Button variant="contained" color="primary">Добавить в GPay</Button>&nbsp;
                                    <Button variant="contained" color="primary">Добавить в Apple Wallet</Button>
                                </div>
                            </TabPanel>
        );
    }

    function contacts() {
        return (
            <>
                                <Typography variant="h5">Контакты</Typography>
                                <p>Код для подтверждения будет отправлена на указанный e-mail, если письмо долго не приходит, проверьте папку «Спам»</p> 
                                <TextField id="Email" label="Email" variant="outlined" fullWidth defaultValue="vasylisa.k@gmail.com" />
                                <p>SMS-код для подтверждения будет отправлен на указанный номер телефона </p>
                                <TextField id="Phone" label="Телефон" variant="outlined" fullWidth defaultValue="+7-999-123-45-67" />
                                <div className={classes.walletButtons}>
                                    <Button variant="contained" color="primary">
                                        Сохранить
                                    </Button>
                                </div> 
            </>
        );
    }

    switch (role) {
        case 'client':
            return(
                <TabContext value={value}>
                                <AppBar position="static" className={classes.tabs} elevation={0}>
                                    <TabList onChange={handleChange} aria-label="simple tabs example" classes={{indicator: classes.indicator}}>
                                        <Tab label="Мои данные" value="1" />                                        
                                        
                                    </TabList>
                                </AppBar>
                                <TabPanel value="1" index="1">  {/* Вкладка Мои данные */}                        
                                    
                                    <form className={classes.contactFields} noValidate autoComplete="off">
                                        <TextField id="ShortName" label="Имя" variant="outlined" fullWidth defaultValue="Василиса Константинопольская" />
                                        {contacts()}                               
                                    </form>                             
                                </TabPanel>
                                
                                
                </TabContext>
            );
        //case 'manager':
        //    return 'Менеджер';
        case 'agent':
            return(
                <TabContext value={value}>
                                <AppBar position="static" className={classes.tabs} elevation={0}>
                                    <TabList onChange={handleChange} aria-label="simple tabs example" classes={{indicator: classes.indicator}}>
                                        <Tab label="Мои данные" value="1" />

                                        <Tab label="СМС" value="3" />
                                    </TabList>
                                </AppBar>
                                <TabPanel value="1" index="1">  {/* Вкладка Мои данные */}                        
                                    
                                    <form className={classes.contactFields} noValidate autoComplete="off">
                                        <TextField id="ShortName" label="Имя, которое будет видеть пользователь" variant="outlined" fullWidth defaultValue="Василиса Константинопольская" />
                                        <TextField id="FullName" label="ФИО" variant="outlined" fullWidth defaultValue="Василиса Константиновна Константинопольская" />

                                        {contacts()}                               
                                    </form>                             
                                </TabPanel>
                                <TabPanel value="2" index="2"> {/* Вкладка Заведение */}  
                                    <SettingsPlace />
                                    
                                </TabPanel>
                                {sms()}
                </TabContext>
            );
        default:              
            return(
                <TabContext value={value}>
                                <AppBar position="static" className={classes.tabs} elevation={0}>
                                    <TabList onChange={handleChange} aria-label="simple tabs example" classes={{indicator: classes.indicator}}>
                                        <Tab label="Мои данные" value="1" />
                                        <Tab label="Заведение" value="2" />
                                        <Tab label="СМС" value="3" />
                                    </TabList>
                                </AppBar>
                                <TabPanel value="1" index="1">  {/* Вкладка Мои данные */}                        
                                    
                                    <form className={classes.contactFields} noValidate autoComplete="off">
                                        <TextField id="ShortName" label="Имя, которое будет видеть гость" variant="outlined" fullWidth defaultValue="Василиса Константинопольская" />
                                        <TextField id="FullName" label="ФИО" variant="outlined" fullWidth defaultValue="Василиса Константиновна Константинопольская" />
                                        <TextField id="Status" label="Статус" variant="outlined" fullWidth defaultValue="Коплю на машину. Осталось немножко — 50 т.р. 🚗" />
                                        {contacts()}                               
                                    </form>                             
                                </TabPanel>
                                <TabPanel value="2" index="2"> {/* Вкладка Заведение */}  
                                    <SettingsPlace />
                                    
                                </TabPanel>
                                {sms()}
                </TabContext>
            );
    } 
    
}

export default SettingsTab;




const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,

    },
    //Аватарка
    userField: {
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
       
    },

    userPic: {
        width: "10rem",
        height: "10rem",
        marginBottom: "1rem",        
    },

    editIcon: {
        background: theme.palette.primary.main,
        border: "2px "+theme.palette.primary.main+" solid",
        color: "#fff",
        boxShadow: ".1rem .1rem .25rem rgba(30,30,30, .3)",
        "&:hover": {
            background: theme.palette.primary.main,
        }
    },

    links: {
      color: theme.palette.primary.main,
      
    },
    
    //ТабПанель
    paper: {
        borderRadius: ".5rem",
    },
    tabs: {
        backgroundColor: "#fff",
        color: "#111",
        "& button": {
          marginTop: "0 !important",
        },
        
    },
    indicator: {
        backgroundColor: "#f47522",
    },
    h3: {
        textAlign: "center",
    },
    walletButtons: {
      textAlign: "center",
      margin: ".5rem 0 2rem 0",
      "& button": {
          marginRight: ".5rem",
          marginBottoms: ".5rem",          

      }
    },
    tabPanel: {
        textAlign: "center",
        '& h3': {            
            marginBottom: "0",
        },
        '& img': {
            margin: "1rem auto",
            display: "block",
        },
        '& p': {            
          textAlign: "left",
        },
        '& b': {            
          textAlign: "center",
          display: "block",
          
        },
        "& button": {
          margin: ".5rem .25rem 0rem .25rem",
        },
        "& form": {
          marginBottom: "1.5rem",
        },

    },
    contactFields: {
        "& .MuiTextField-root": {
            marginBottom: "1rem",
        }
    }
    
  }));