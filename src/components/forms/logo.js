//Вывод логотипа в формах
import React from 'react';
import { makeStyles} from '@material-ui/core/styles';

import logo from'../../assets/logo.svg'; //Логотип Dovolen

const useStyles = makeStyles((theme) => ({
    logo: {
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        color: "#999",        
        paddingBottom: "1rem",
        fontSize: ".8rem",
        "& img": {
            width: "35%",
        },

    } 

}));

function Logo() {
    const classes = useStyles();
    return(
        <div className={classes.logo}>
            <img src={logo} alt="Dovolen.me" />
            Безналичная благодарность                            
        </div>
    );
}

export default Logo;