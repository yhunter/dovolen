import React from 'react';

import {BrowserRouter as Router, Route} from 'react-router-dom';
import { ParallaxProvider } from 'react-scroll-parallax';
import {Helmet} from "react-helmet";

import Landing from './screens/landing'; //Лендинг
import Profile from './screens/profile'; //Личный кабинет
import Pay from './screens/pay'; //Форма оплаты
import Success from './screens/success'; //Успешная оплата
import Login from './screens/login'; //Форма логина
import Signup from './screens/signup'; //Форма регистрации


function App() {
  return (
    <ParallaxProvider>
      <Router basename="temp/dovolen/">
          <Helmet>
              <meta charSet="utf-8" />
              <title>dovolen — Безналичная благодарность</title>
              {/*<link rel="canonical" href="" />*/}
          </Helmet>
          <div className="App viewport">         
                <Route path='/' component={Landing} exact /> {/* Лендинг */}                
                <Route path='/pay' component={Pay} exact /> {/* Форма оплаты */}
                <Route path='/success' component={Success} exact /> {/* Успешная оплата */}    
                <Route path='/login' component={Login} exact /> {/* Форма логина */} 
                <Route path='/signup' component={Signup} exact /> {/* Форма регистрации */}     
                <Route path='/profile' component={Profile} exact /> {/* Личный кабинет, главная */}
                <Route path='/profile/:page' component={Profile} /> {/* Личный кабинет, остальные страницы */}  

                {/* Заглушки для проброса разных ролей в личный кабинет */}
                <Route path='/client' component={Profile} exact /> {/* Личный кабинет клиента, главная */}
                <Route path='/client/:page' component={Profile}  /> {/* Личный кабинет клиента, остальные страницы */}  

                <Route path='/manager' component={Profile} exact /> {/* Личный кабинет менеджера, главная */}
                <Route path='/manager/:page' component={Profile}  /> {/* Личный кабинет менеджера, остальные страницы */} 

                <Route path='/agent' component={Profile} exact /> {/* Личный кабинет агента, главная */}
                <Route path='/agent/:page' component={Profile}  /> {/* Личный кабинет агента, остальные страницы */}    
          </div>
      </Router>
    </ParallaxProvider>
  );
}

export default App;
