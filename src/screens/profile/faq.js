//Компонент страницы Ответы и вопросы
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {Accordion, AccordionSummary, AccordionDetails, Typography, Grid} from '@material-ui/core';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      display: "block",
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    answer: {
        display: "block",
    }
  }));

function Faq() {
    const classes = useStyles();
    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.root}> 
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className={classes.heading}>
                            Опишите работу сервиса
                        </Typography>
                    </AccordionSummary>
                    <AccordionDetails  className={classes.answer}>                        
                        <h3>Как это работает?</h3>                       
                        
                        <ul>
                            <li>Гость наводит камеру на QR-код получателя чаевых.</li>
                            <li>Переходит по ссылке и оставляет чаевые: Банковской картой, Apple Pay, Google Pay, Samsung Pay.</li>
                        </ul>                        

                        <p>QR код может быть размещен на пластиковой карте, брендированной визитке, стикере, тейбл-тенте или предварительном чеке.</p>

                        <p>При регистрации в сервисе получателю чаевых выпускается виртуальная карта на которую моментально зачисляются чаевые.</p>

                        <p>Карту можно добавить в Wallet, использовать для оплаты покупок в интернет или снимать наличные путем перевода на карту любого банка РФ.
                    </p>
                    </AccordionDetails>
                </Accordion>
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel2a-content"
                        id="panel2a-header"
                    >
                        <Typography className={classes.heading}>Какая комиссия сервиса?</Typography>
                    </AccordionSummary>
                    <AccordionDetails  className={classes.answer}>
                        <p>
                            Комиссия за зачисление чаевых — 5%.
                        </p>

                        <p>
                            Оплата покупок и платежи в интернете - без комиссии.
                        </p>    

                        <p>
                            Запрос денег из приложения другого банка бесплатно или по тарифам банка эмитента.
                        </p> 

                        <p>Перевод денег на другую карту 1% минимум 30 рублей.
                        </p>
                    </AccordionDetails>
                </Accordion>
                
                <Accordion>
                    <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel2a-content"
                        id="panel2a-header"
                    >
                        <Typography className={classes.heading}>Как снять наличные с виртуальной карты?</Typography>
                    </AccordionSummary>
                    <AccordionDetails  className={classes.answer}>
                        <h3>Вариант 1. Оплата смартфоном в магазине - Комиссия 0%</h3>
                        <p>
                            Добавьте карту карту «Dovolen» в Wallet и используйте для оплаты покупок телефоном.
                        </p>
                        <h3>Вариант 2. Снять наличные с карты - Комиссия 0%*</h3>
                        <p>
                            Запустите приложение Вашего банка и запросите деньги, указав в качестве карты для списания средств, реквизиты вашей виртуальной карты.
                            * Банки иногда начисляют комиссию за подобные переводы

                        </p>
                            <h3>Вариант 3. Перевод с карты на карту - Комиссия 1% мин 30 рублей</h3>
                        <p>
                            В личном кабинете выберите «Перевод с карты на карту».
                        </p>
                            <h3>Вариант 4. Покупки в интернет - Комиссия 0%</h3>
                        <p>
                            Используйте виртуальную карту «Dovolen» для покупок в интернет.
                        </p>

                    </AccordionDetails>
                </Accordion>                
            </Grid>
        </Grid>
    );
}

export default Faq;