//Храним структуру подразделов и меню
import React from 'react';

//Иконки для меню
import FaqIcon from '@material-ui/icons/LiveHelp';
import ContactIcon from '@material-ui/icons/ContactMail';
import SupportIcon from '@material-ui/icons/HeadsetMic';
import PrintIcon from '@material-ui/icons/Print';
import CardIcon from '@material-ui/icons/CreditCard';
import HistoryIcon from '@material-ui/icons/History';
import HomeIcon from '@material-ui/icons/Home';
import SettingsIcon from '@material-ui/icons/AccountCircle';
import HomeWorkIcon from '@material-ui/icons/HomeWork';
import TimelineIcon from '@material-ui/icons/Timeline';




//Компоненты для контента
import Main from './main.js'; //Контент главной страницы
import History from './history.js'; //Контент страницы истории баланса
import Details from './details.js'; //Контент страницы реквизиты
import Prints from './prints.js'; //Контент страницы печатные формы
import Faq from './faq.js'; //Контент страницы вопросы и ответы
import Support from './support.js'; //Контент страницы техподдержки
import Contacts from './contacts.js'; //Контент страницы контакты 
import Settings from './settings.js'; //Контент страницы настройки
import Withdraw from './withdraw.js'; //Контент страницы настройки
import Tips from './tips.js'; //Контент страницы история чаевых у клиента
import Place from './place.js'; //Контент страницы заведение у менеджера
import Analytics from './analytics.js'; //Контент страницы заведение у менеджера

function menuItems(role="profile") {
    
    return(
        {
            "profile": {//Меню получателя
                "menu1": [
                    "main", "settings", "history", "details", "prints" //Верхние пункты
                ],
                "menu2": [
                    "faq", "support", "contacts" //Нижние пункты
                ]
            },
            "manager": {//Меню менеджера
                "menu1": [
                    "main", "settings", "place", "analytics", "history", "details", "prints" //Верхние пункты
                ],
                "menu2": [
                    "faq", "support", "contacts" //Нижние пункты
                ]
            },
            "agent": {//Меню агента
                "menu1": [
                    "main", "settings", "places", "history", "details", "prints" //Верхние пункты
                ],
                "menu2": [
                    "faq", "support", "contacts" //Нижние пункты
                ]
            },
            "client": {//Меню клиента
                "menu1": [
                    "tips", "settings" //Верхние пункты
                ],
                "menu2": [
                    "faq", "support", "contacts" //Нижние пункты
                ]
            },
            "main": {          
                "name": "Главная",
                "component": (<Main role={role} />),
                "icon": (<HomeIcon/>)
            },
            "history": {
                "name": "История баланса",
                "component": (<History role={role} />),
                "icon": (<HistoryIcon/>)            
            },
            "tips": {
                "name": "История чаевых",
                "component": (<Tips />),
                "icon": (<HistoryIcon/>)            
            },
            "details":{
                "name": "Реквизиты",
                "component": (<Details/>),
                "icon": (<CardIcon/>)
            },
            "prints":{
                "name": "Печатные формы",
                "component": (<Prints/>),
                "icon": (<PrintIcon/>)
            },
            "faq":{
                "name": "Вопросы и ответы",
                "component": (<Faq role={role} />),
                "icon": (<FaqIcon/>)
            },
            "support": {            
                "name": "Поддержка",
                "component": (<Support role={role} />),
                "icon": (<SupportIcon/>)
            },
            "contacts":{
                "name": "Контакты",
                "component": (<Contacts role={role} />),
                "icon": (<ContactIcon/>)
            },
            "settings":{
                "name": "Мои настройки",
                "component": (<Settings role={role} />),
                "icon": (<SettingsIcon />)
            },
            "place": {          
                "name": "Заведение",
                "component": (<Place role={role} />),
                "icon": (<HomeWorkIcon/>)
            },
            "places": {          
                "name": "Подключенные заведения",
                "component": (<Place role={role} />),
                "icon": (<HomeWorkIcon/>)
            },
            "analytics": {          
                "name": "Аналитика",
                "component": (<Analytics />),
                "icon": (<TimelineIcon/>)
            },
            "withdraw": {          
                "name": "Вывод на карту",
                "component": (<Withdraw />),

            },
        }
    );    

}

export default menuItems;