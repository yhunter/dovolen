//Главная страница профиля, выбираем контент для отображения
import React from 'react';

import MainRecepient from '../../components/profile/mainRecepient'; //Компонент главной страницы профиля получателей
import MainClient from '../../components/profile/mainClient.js'; //Компонент главной страницы профиля клиентов

function Main(props) {
    const role = props.role;
    switch (role) {
      case 'client':
          return(
              <>
                <MainClient role={role} />
              </>
          );
      default: 
          return (        
              <>
                <MainRecepient role={role} />
              </> 
          );
    }    
}

export default Main;