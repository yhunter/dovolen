//Компонент страницы Реквизиты
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Typography, Grid, Button, Select, MenuItem, FormControl, InputLabel, OutlinedInput, InputAdornment} from '@material-ui/core';

import Card from '../../components/profile/card.js'; //Компонент отрисовки банковской карты

function Withdraw() {
    const classes = useStyles();
    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.center}>
                <Typography variant="h5">
                    Виртуальная карта
                </Typography>
                <div className={classes.center}>
                    <Card /> {/* Компонент отрисовки банковской карты */}
                    <Typography variant="h5">
                        Банковская карта
                    </Typography>
                    <FormControl   className={classes.formControl} fullWidth>
                        <InputLabel id="demo-simple-select-label">Карта для вывода</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            defaultValue={1}                            
                            >
                            <MenuItem value={1}>Название карты (•••• 5796)</MenuItem>
                            <MenuItem value={2}>Название карты (•••• 1476)</MenuItem>
                            <MenuItem value={3}>Название карты (•••• 9130)</MenuItem>
                        </Select>
                    </FormControl>
                    <div className={classes.sum}>
                        <FormControl  className={classes.sumInput} variant="outlined">
                            <InputLabel htmlFor="outlined-adornment-amount">Сумма для вывода</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-amount"
                                type="number"
                                endAdornment={<InputAdornment position="end">₽</InputAdornment>}
                                labelWidth={150}
                            />
                        </FormControl>
                        <Button variant="contained" color="primary" size="large">
                            Вывести
                        </Button>

                    </div>
                </div>
            </Grid>
        </Grid>
    );
}

export default Withdraw;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      "& a:visited": {
        color: "#f33",
      }
    },
    formControl: {
        margin: "0 0 1rem 0",
        minWidth: 120,
    },
    
    center: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        "& h5": {
            marginBottom: "1rem",
        }
    },

    walletButtons: {
        margin: "0rem auto 2rem auto",
        textAlign: "center",
        "& button": {
          marginRight: ".5rem", 
          marginBottom: ".5rem",         
        },
    },

    links: {
      color: theme.palette.primary.main,      
    },   
    sum: {
        display: "flex",
        flexDirection: "row",
        margin: 0,
        width: "100%",
    },
    sumInput: {
        marginRight: ".5rem",
        flex: 1,
    }
    
  }));