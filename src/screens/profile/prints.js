import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import PrintForm from '../../components/profile/printForm'; //Компонент отдельной печатной формы

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      display: "block",
    },
    cell: {
        "& .MuiGrid-item": {
            marginBottom: "1.5rem",
        }
        
    }
  }));

function Prints() {
    const classes = useStyles();
    return(
        <Grid container spacing={3}  className={classes.cell}>
            <Grid item xs={12} md={12} lg={6}>
                <PrintForm /> {/*Компонент отдельной печатной формы  */}
            </Grid>
            <Grid item xs={12} md={12} lg={6}>
                <PrintForm /> {/*Компонент отдельной печатной формы  */}
            </Grid>
            <Grid item xs={12} md={12} lg={6}>
                <PrintForm /> {/*Компонент отдельной печатной формы  */}
            </Grid>
            <Grid item xs={12} md={12} lg={6}>
                <PrintForm /> {/*Компонент отдельной печатной формы  */}

            </Grid>
            <Grid item xs={12} md={12} lg={6}>
                <PrintForm /> {/*Компонент отдельной печатной формы  */}

            </Grid>
        </Grid>        
    );
}

export default Prints;