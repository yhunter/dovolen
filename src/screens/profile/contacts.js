//Компонент вкладки Контакты
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';

import {Grid, Button, Typography} from '@material-ui/core';

import AgentContact from '../../components/profile/agentContacts'; //Контакты агента

import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      "& a:visited": {
        color: "#f33",
      }
    },

    blocks: {
        "& h5": {
            padding: "0 0 0 0",
        },
        "& a": {
            margin: "0 .5rem .5rem 0",
        },
        "& p": {
            marginBottom: "1.5rem",
        },
        "& svg": {
            verticalAlign: "middle",
            fill: "rgba(0, 0, 0, 0.54)",
        }
    },

    links: {
      color: theme.palette.secondary.main,
      
    },    
    
  }));

function Contacts(props) {
    const classes = useStyles();
    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.blocks}>  
                <AgentContact role={props.role} />              
                <Typography variant="h5" m="3">
                  <PhoneIphoneIcon fontSize="small" /> <a href="tel:+79991234567" className={classes.links}  target="_blank" rel="noopener noreferrer"> +7-999-123-45-67</a> 
                </Typography>
                <p>
                    <Button variant="contained" color="primary" to="tel:+79991234567" component={Link}  target="_blank" rel="noopener noreferrer">Позвонить</Button>
                    <nobr>
                      <Button variant="outlined" color="secondary" to="" component={Link}  target="_blank" rel="noopener noreferrer">Telegram</Button>
                      <Button variant="outlined" color="secondary" to="" component={Link}  target="_blank" rel="noopener noreferrer">Whatsapp</Button>
                    </nobr>
                </p>
                <Typography variant="h5">
                  <AlternateEmailIcon fontSize="small"  /> <a href="mailto:mailbox@dovolen.me" className={classes.links}  target="_blank" rel="noopener noreferrer"> mailbox@dovolen.me</a>
                </Typography>
                <p>                    
                    <Button variant="contained" color="primary" to="/profile/support" component={Link}>Сообщение с сайта</Button>
                </p>                

            </Grid>
        </Grid>
    );
}

export default Contacts;