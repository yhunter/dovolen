//Компонент страницы История баланса
import React, { Fragment, useState  } from "react";
import { KeyboardDatePicker } from "@material-ui/pickers";
import { makeStyles } from '@material-ui/core/styles';
import {Grid, Button} from '@material-ui/core';

import MomentUtils from "@date-io/moment"; //Для работы датапикеров
import moment from "moment"; //Для работы датапикеров
import "moment/locale/ru"; //Локализации для датапикеров
import {  MuiPickersUtilsProvider} from '@material-ui/pickers'; //Для работы датапикеров

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'; //Иконки кнопок навигации
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft'; //Иконки кнопок навигации

import Balance from '../../components/profile/balance.js'; // Баланс и изображение карточки
import LastEvents from '../../components/profile/lastEvents.js'; // Таблица последних изменений баланса 

moment.locale("ru");



function History(props) {
  const classes = useStyles();
  const [locale] = useState("ru");

  
  //Первый датапикер
  const [selectedDate, setDate] = useState(moment());
  const [inputValue, setInputValue] = useState(moment().format("DD.MM.YYYY"));

  const onDateChange = (date, value) => {
    setDate(date);
    setInputValue(value);
  };

  //Второй датапикер
  const [selectedDate2, setDate2] = useState(moment());
  const [inputValue2, setInputValue2] = useState(moment().format("DD.MM.YYYY"));

  const onDateChange2 = (date, value) => {
    setDate2(date);
    setInputValue2(value);
  };

  const dateFormatter = str => {
    return str;
  };  
    
    
    return(
      <Fragment>
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={8} xl={8}>
                <Balance role={props.role} />  {/* Баланс и изображение карточки */}   
          
                <h2>События:</h2>
                                   
                    
                    <MuiPickersUtilsProvider  libInstance={moment} utils={MomentUtils} locale={locale}>
                        <Grid container spacing={0}>
                            <Grid item xs={12} md={12} lg={6} xl={6}> 
                              <div className={classes.mb}>    
                                <KeyboardDatePicker
                                    autoOk={true}
                                    showTodayButton={true}
                                    todayLabel="Сегодня"
                                    cancelLabel="Отмена"
                                    value={selectedDate}
                                    label="Начало диапазона"
                                    format="DD.MM.YYYY"
                                    inputValue={inputValue}
                                    onChange={onDateChange}
                                    rifmFormatter={dateFormatter}
                                  />
                                </div>
                            </Grid>
                            <Grid item xs={12} md={12} lg={6} xl={6}>
                              <div className={classes.mb}> 
                                  <KeyboardDatePicker
                                        autoOk={true}
                                        showTodayButton={true}
                                        todayLabel="Сегодня"
                                        cancelLabel="Отмена"
                                        label="Конец диапазона"
                                        value={selectedDate2}
                                        format="DD.MM.YYYY"
                                        inputValue={inputValue2}
                                        onChange={onDateChange2}
                                        rifmFormatter={dateFormatter}
                                  />
                              </div>
                            </Grid> 
                        </Grid>       
                        
                    </MuiPickersUtilsProvider>
                    
                
                <LastEvents role={props.role} /> {/* Таблица последних изменений баланса */}
                <div className={classes.centerText}>
                    <Button variant="contained" color="primary" startIcon={<KeyboardArrowLeftIcon />}>
                        Раньше
                    </Button>
                    <Button variant="contained" color="primary"  endIcon={<KeyboardArrowRightIcon />}>
                        Позже
                    </Button>
                </div>
                
            </Grid>
        </Grid>
        </Fragment>  
    );
}

export default History;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',    
  },
  a: {
    color: theme.palette.primary.main,
  },    
  
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  paper: {
    borderRadius: ".5rem",
  },
  centerText: {
    textAlign: "center",
    "& button": {
        margin: ".5rem",
    }
  },
  mb: {
    margin: "0 0 1rem  0",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",

  }

}));