//Компонент страницы техподдержки
import React from 'react';
import {TextField, Grid, Button} from '@material-ui/core';

import AgentContact from '../../components/profile/agentContacts'; //Контакты агента

function Support(props) {
    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6}>
                <AgentContact role={props.role} />
                <p>Если у&nbsp;вас возникли проблемы с&nbsp;спользованием сервиса, вы&nbsp;можете задать свой вопрос в&nbsp;службу поддержки.</p>
                <TextField
                    id="outlined-multiline-static"
                    label="Ваше обращение"
                    multiline
                    fullWidth
                    rows={10}
                    defaultValue=""
                    variant="outlined"
                />
                <p style={{"textAlign": "center"}}>
                    <Button variant="contained" color="primary">
                    Отправить сообщение
                    </Button>
                </p>
            </Grid>
        </Grid>
    );
}

export default Support;