//Компонент страницы Реквизиты
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Typography, Grid, Button} from '@material-ui/core';

import Card from '../../components/profile/card.js'; //Компонент отрисовки банковской карты
import AddCard from '../../components/profile/addCard.js'; //Компонент привязки банковской карты

function Details() {
    const classes = useStyles();

    const [cardsDetails, CardsNumber] = React.useState({"number": 1}); //Стейт с количеством реквизитов карт

    const addNewCard = () => {//Добавление карты
        CardsNumber({
            "number": cardsDetails.number+1,
        });        
    };

    const CardsDetails = () => { //Формируем массив объектов для ввода рекивзитов каждой карты
        let i = 0;
        let output = [];
        while (i < cardsDetails.number) { // выводит 0, затем 1, затем 2
            output.push(<AddCard />);
            i++;
        }
        return(
                 output   
        );
    }

    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={6} className={classes.center}>
                <Typography variant="h5">
                    Виртуальная карта
                </Typography>
                <div>
                    <Card /> {/* Компонент отрисовки банковской карты */}
                </div>
                <Button variant="outlined" color="secondary">
                    Показать реквизиты
                </Button>
                
            </Grid>

            <Grid item xs={12} md={12} lg={6} className={classes.center}>
                <Typography variant="h5">
                    Банковская карта
                </Typography>
                <div>
                    {CardsDetails()} {/* Вывод карт для ввода реквизитов */}
                </div>
                <Button variant="outlined" color="secondary" onClick={addNewCard}>
                    Добавить новую карту
                </Button>
            </Grid>
            
        </Grid>
    );
}

export default Details;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      "& a:visited": {
        color: "#f33",
      }
    },
    
    center: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        "& h5": {
            marginBottom: "1rem",
        }
    },

    walletButtons: {
        margin: "0rem auto 2rem auto",
        textAlign: "center",
        "& button": {
          marginRight: ".5rem", 
          marginBottom: ".5rem",         
        },
    },

    links: {
      color: theme.palette.primary.main,      
    },    
    
  }));