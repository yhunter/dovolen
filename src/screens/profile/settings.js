//Компонент страницы Мои настройки
import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
//import { Link } from 'react-router-dom';

import {Grid, Badge, Avatar,  Typography, IconButton,  Paper} from '@material-ui/core';

import EditIcon from '@material-ui/icons/Edit';

import SettingsTabs from '../../components/profile/settingsTabs';

import ava from '../../assets/courtney-cook-le7D9QFiPr8-unsplash.jpg';

function Settings(props) {
    const classes = useStyles();

    const role = props.role;

    const Perсent = () => {
        if (role==='agent') {
            return (
                <div className={classes.percent}>
                    <Typography variant="h3">15%</Typography>
                    <Typography variant="subtitle1" style={{"margin": "0 20%"}}>Ваш текущий процент агентских вознаграждений</Typography>                
                </div>
            );
        }
        else {
            return (<></>);
        }
    }

    const roleName = () => { //Выводим роль по-русски
        switch (role) {
            case 'client':
              return 'Клиент';
            case 'manager':
                return 'Менеджер';
            case 'agent':
                return 'Агент';
            default:              
              return 'Получатель';
        }        
    }

    return(
        <Grid container spacing={3}>
            <Grid item xs={12} md={12} lg={4} className={classes.userField}>
                <Badge
                    overlap="circle"
                    anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                    badgeContent={<IconButton aria-label="edit" className={classes.editIcon} size="small"><EditIcon  /></IconButton>}
                >  
                    <Avatar alt="Василиса Константинопольская" src={ava} className={classes.userPic} />
                </Badge>
                <Typography variant="h5">Василиса Константинопольская</Typography>
                <Typography variant="subtitle1">{roleName()}</Typography>
                <Perсent />
            </Grid>
            <Grid item xs={12} md={12} lg={6}> 
                <Paper className={classes.paper} elevation={1}> 
                    <SettingsTabs role={role} />
                </Paper>    
            </Grid>
        </Grid>
    );
}

export default Settings;

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,

    },
    //Аватарка
    userField: {
        textAlign: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
       
    },

    userPic: {
        width: "10rem",
        height: "10rem",
        marginBottom: "1rem",        
    },

    editIcon: {
        background: theme.palette.primary.main,
        border: "2px "+theme.palette.primary.main+" solid",
        color: "#fff",
        boxShadow: ".1rem .1rem .25rem rgba(30,30,30, .3)",
        "&:hover": {
            background: theme.palette.primary.main,
        }
    },

    links: {
      color: theme.palette.primary.main,
      
    },
    
    //ТабПанель
    paper: {
        borderRadius: ".5rem",
    },
    tabs: {
        backgroundColor: "#fff",
        color: "#111",
        "& button": {
          marginTop: "0 !important",
        },
        
    },
    indicator: {
        backgroundColor: "#f47522",
    },
    h3: {
        textAlign: "center",
    },
    walletButtons: {
      textAlign: "center",
      margin: ".5rem 0 2rem 0",
      "& button": {
          marginRight: ".5rem",
          marginBottoms: ".5rem",          

      }
    },
    tabPanel: {
        textAlign: "center",
        '& h3': {            
            marginBottom: "0",
        },
        '& img': {
            margin: "1rem auto",
            display: "block",
        },
        '& p': {            
          textAlign: "left",
        },
        '& b': {            
          textAlign: "center",
          display: "block",
          
        },
        "& button": {
          margin: ".5rem .25rem 0rem .25rem",
        },
        "& form": {
          marginBottom: "1.5rem",
        },

    },
    contactFields: {
        "& .MuiTextField-root": {
            marginBottom: "1rem",
        }
    },
    percent: {
        marginTop: "2rem",
    }
    
  }));