//Компонент Заведение для менеджера
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {FormControl, InputLabel, Select, MenuItem, Typography, Grid, Button} from '@material-ui/core';
import Slider from '@material-ui/core/Slider';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import PlaceWorkers from '../../components/profile/placeWorkers'; //Список сотрудников
import AddplaceFields from '../../components/profile/addPlaceFields'; //Поля добавления заведения в диалоговое окно



function Place(props) {
    const classes = useStyles();

    const [dialogStatus, setOpen] = React.useState({"open": false}); //Состояние диалогового окна добавления заведения

    const handleClickOpen = () => {
        setOpen({
            "open": true,
        });
    };

    const handleClose = () => {
        setOpen({"open": false});
    };

    const PlaceHeader = (role) => { //Отображение хедера для компонента. Либо название заведения, либо выпадающий список для агента
        if (role==='agent') {
            return (
                <Grid container spacing={1}>
                                        <Grid item xs={12} md={12} lg={4} align="center" >
                                            <FormControl variant="outlined" >
                                                <InputLabel id="placeSelectLabel">Заведение</InputLabel>
                                                <Select
                                                    labelId="placeSelectLabel"
                                                    id="wplaceSelect"
                                                    style={{"minWidth": "16rem"}}
                                                    
                                                    label="Тип заведения"
                                                    defaultValue="0"
                                                    //onChange={handleChange}
                                                >
                                                    <MenuItem value="0">«Пятый океан», г. Владивосток</MenuItem>
                                                    <MenuItem value="1">«Сапиенс эст», г. Москва</MenuItem>
                                                    <MenuItem value="2">«Дача», г. Тамбов</MenuItem>


                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={12} lg={8} className={classes.newPlace} >
                                            <Button color="primary" variant="outlined" onClick={handleClickOpen}>+ Новое заведение</Button>
                                        </Grid>
                                        
                                        <Dialog open={dialogStatus.open} onClose={handleClose} aria-labelledby="form-dialog-title">
                                            <DialogTitle id="form-dialog-title">Новое заведение</DialogTitle>
                                            <DialogContent className={classes.dialogContent}>
                                                <AddplaceFields required="true" />
                                            </DialogContent>
                                            <DialogActions>
                                            <Button onClick={handleClose} color="primary" variant="contained">
                                                Сохранить
                                            </Button>
                                            <Button onClick={handleClose} color="primary" >
                                                Отмена
                                            </Button>
                                            </DialogActions>
                                        </Dialog>
                </Grid>
            );
        }
        else {
            return (
                <Typography variant="h5" className={classes.header} >«Art-Caviar»</Typography>
            );
        }
    } 

    return (
        <div> 
            <Grid container spacing={3} className={classes.table}>
                <Grid item xs={12} md={12} lg={8}>
                    {PlaceHeader(props.role)}
                </Grid>      
            </Grid>         

            <Grid container spacing={3} className={classes.table}>  
                
                <Grid item xs={12} md={12} lg={8}>                    
                    <PlaceWorkers workers={workers} />
                </Grid>   
                <Grid item xs={12} md={12} lg={4}>
                        <div style={{"textAlign": "center", "fontSize": "1.25rem"}}>
                            Процент чаевых официанту
                        </div>
                    <div style={{"margin": "2rem 2rem 0 2rem"}}>

                        <Slider
                            defaultValue={100}
                            getAriaValueText={valuetext}
                            aria-labelledby="discrete-slider-always"
                            step={1}
                            marks={marks}
                            valueLabelDisplay="on"
                        />
                    </div>
                </Grid>  
            </Grid>
        </div>
    );
}

export default Place;

//Для слайдера  
function valuetext(value) {
    return `${value}%`;
};

const marks = [
    {
      value: 0,
      label: '0%',
    },
    {
      value: 50,
      label: '50%',
    },
    {
      value: 100,
      label: '100%',
    },
  
];

const workers = [ //Тестовые данные
        {
            "name": "Василиса Константинопольская",
            "tel": "+7123456789",
            "email": "mailbox@gmail.com",
            "position": "Официант"
        },
        {
            "name": "Василиса Константинопольская",
            "tel": "+7123456789",
            "email": "mailbox@gmail.com",
            "position": "Официант"
        },
        {
            "name": "Иван Иванов",
            "tel": "+7123456789",
            "email": "mailbox@gmail.com",
            "position": "Официант"
        },
];


const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      display: "block",
    },
    header: {
        textAlign: "center",
        [theme.breakpoints.up('md')]: {
            textAlign: "left",
        }
    },
    table: {
        marginTop: ".75rem",
        [theme.breakpoints.down('md')]: {
            flexDirection: "column-reverse",
        }
    },
    newPlace: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        [theme.breakpoints.up('md')]: {           
            
            justifyContent: "flex-end",
        }

    },
    dialogContent: {
        "& .MuiFormControl-root": {
            marginBottom: "1rem",
        }
    }
}));
