//Компонент Аналитика для менеджера
import React, { useState  }  from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {FormControl, InputLabel, Select, MenuItem, Typography, Grid} from '@material-ui/core';

import Rating from '@material-ui/lab/Rating';
import { KeyboardDatePicker } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment"; //Для работы датапикеров
import moment from "moment"; //Для работы датапикеров
import "moment/locale/ru"; //Локализации для датапикеров
import {  MuiPickersUtilsProvider} from '@material-ui/pickers'; //Для работы датапикеров

import AnalyticsManager from '../../components/profile/analyticsManager';



moment.locale("ru");



const data = [
        {
            "date": "20.09.2020 18:30",
            "name": "Василиса Константинопольская",
            "rating": 5,
            "tips": 700,            
            "response": "Замечательное обслуживане, обязательно зайду еще!",
            "author": "Артемий"
            
        },
        {
            "date": "15.09.2020 15:27",
            "name": "Василиса Константинопольская",
            "rating": 4,
            "tips": 400,
            "response": "",
            "author": ""

        },
        {
            "date": "9.08.2020 11:54",
            "name": "Иван Иванов",
            "rating": 4,
            "tips": 300,
            "response": "Все хорошо, спасибо =)",
            "author": "Федор Новоселов"          
            
        },
        
];





function Analytics(props) {
    const classes = useStyles();
    const [locale] = useState("ru");

    //Первый датапикер
    const [selectedDate, setDate] = useState(moment());
    const [inputValue, setInputValue] = useState(moment().format("DD.MM.YYYY"));

    const onDateChange = (date, value) => {
        setDate(date);
        setInputValue(value);
    };

    //Второй датапикер
    const [selectedDate2, setDate2] = useState(moment());
    const [inputValue2, setInputValue2] = useState(moment().format("DD.MM.YYYY"));

    const onDateChange2 = (date, value) => {
        setDate2(date);
        setInputValue2(value);
    };

    const dateFormatter = str => {
        return str;
    };  
    return (
        <div>
            <Typography variant="h5" className={classes.header} >«Art-Caviar»</Typography>
            <div className={classes.subHeader}>
                    <MuiPickersUtilsProvider  libInstance={moment} utils={MomentUtils} locale={locale}>
                        <Grid container spacing={3}>
                            <Grid item xs={12} md={12} lg={8} xl={8} align="center"> 
                                <Grid container spacing={1}>
                                        <Grid item xs={12} md={12} lg={4}  >
                                            <FormControl variant="outlined"   required={props.required} >
                                                <InputLabel id="workerSelectLabel">Сотрудник</InputLabel>
                                                <Select
                                                    labelId="workerSelectLabel"
                                                    id="workerSelect"
                                                    style={{"minWidth": "16rem"}}
                                                    
                                                    label="Тип заведения"
                                                    defaultValue="0"
                                                    //onChange={handleChange}
                                                >
                                                    <MenuItem value="0">Все</MenuItem>
                                                    <MenuItem value="2">Василиса Константинопольская</MenuItem>
                                                    <MenuItem value="3">Иван Иванов</MenuItem>

                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} md={12} lg={4}  align="center"> 
                                            <div className={classes.mb}>    
                                                <KeyboardDatePicker
                                                    autoOk={true}
                                                    showTodayButton={true}
                                                    todayLabel="Сегодня"
                                                    cancelLabel="Отмена"
                                                    value={selectedDate}
                                                    label="Начало диапазона"
                                                    format="DD.MM.YYYY"
                                                    inputValue={inputValue}
                                                    onChange={onDateChange}
                                                    rifmFormatter={dateFormatter}
                                                />
                                            </div>
                                        </Grid>
                                        <Grid item xs={12} md={12} lg={4}  align="center">
                                                <div className={classes.mb}> 
                                                    <KeyboardDatePicker
                                                            autoOk={true}
                                                            showTodayButton={true}
                                                            todayLabel="Сегодня"
                                                            cancelLabel="Отмена"
                                                            label="Конец диапазона"
                                                            value={selectedDate2}
                                                            format="DD.MM.YYYY"
                                                            inputValue={inputValue2}
                                                            onChange={onDateChange2}
                                                            rifmFormatter={dateFormatter}
                                                    />
                                                </div>
                                        </Grid>

                                </Grid>
                            </Grid>
                            
                        </Grid>       
                        
                    </MuiPickersUtilsProvider>
            </div>
            
        
            <Grid container spacing={3} className={classes.table}>
                
                
                <Grid item xs={12} md={12} lg={8}>
                    
                    <AnalyticsManager data={data} />
                </Grid>   
                <Grid item xs={12} md={12} lg={4} className={classes.stat}>
                        <div className={classes.statTitle}>
                            Статистика
                        </div>                        
                        <div className={classes.statRating}>
                            <label>Средряя оценка: 4.3</label>
                            <Rating                                 
                                value={4.3} 
                                readOnly 
                                size="large"
                            />                        
                        </div>
                        <div className={classes.statTips}>
                            <div className={classes.statAverage}>
                                <label>Средние чаевые</label>
                                500 ₽
                            </div>
                            <div className={classes.statAll}>
                                <label>Сумма</label>
                                5300 ₽
                            </div>
                        </div>

                </Grid>  
            </Grid>
        </div>
    );
}

export default Analytics;

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      display: "block",
    },
    header: {
        textAlign: "center",
        
        [theme.breakpoints.up('md')]: {
            textAlign: "left",
        }
    },
    subHeader: {
        marginTop: "1rem",
    },
    table: {
        marginTop: ".25rem",
        [theme.breakpoints.down('md')]: {
            flexDirection: "column-reverse",
        }
    },
    stat: {
        textAlign: "center",
        color: "#696983",
        fontSize: "1.5rem",
        "& label": {
            fontSize: ".8rem",
        },
    },
    statTitle: {
        fontSize: "1.5rem",
    },
    statRating: {
        margin: ".5rem 0 1rem 0",
        display: "flex",
        flexDirection: "column-reverse",
        alignItems: "center",
    },
    statTips: {
        display: "flex",
        flexDirection: "row"
    },
    statAverage: {
        display: "flex",
        flexDirection: "column-reverse",
        flex: 1,
    },
    statAll: {
        display: "flex",
        flexDirection: "column-reverse",
        flex: 1,
    },
}));