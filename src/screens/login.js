//Форма логина
import React from 'react';
import { makeStyles, useTheme, createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import {TextField, Button} from '@material-ui/core'; 
import { Link } from 'react-router-dom';

import FormControl from '@material-ui/core/FormControl';

import NumberFormat from 'react-number-format';//Для маски номера телефона

import 'fontsource-roboto';

import Logo from'../components/forms/logo'; //Логотип Dovolen
import bg from '../assets/formBg.jpg'; //Фоновая картинка

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      alignItems: "center",
      justifyContent: "left",
      
      [theme.breakpoints.up('lg')]: {
        backgroundColor: "#666",  
        backgroundImage: 'url('+bg+')',
        backgroundSize: "cover",
        backgroundPosition: "50% 50%",          

      },
      
      minHeight: "100vh",    
    },  
    leftCol: {
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",

        [theme.breakpoints.up('lg')]: {
            width: "50%",             

         },
    },
    wrapper: {
        width: "100%",
        [theme.breakpoints.up('lg')]: {
            width: "320px",
            padding: "1rem",

         },
    },

    userCard: {
        overflow: "hidden",
        display: "block",
        backgroundColor: "#fff",
        
        [theme.breakpoints.up('lg')]: {
            borderRadius: "1rem",
            boxShadow: ".5rem .5rem 1rem rgba(30,30,30,.5)",           
         },
        padding: "1rem",
        "& button, .MuiFormControl-root, .MuiPaper-root": {
            margin: ".5rem 0"
        },
        "& p": {
            fontSize: ".75rem",
            textAlign: "center"
        },

    },

    buttons: {
        textAlign: "center",
        display: "flex",
        margin: "0 0 .5rem 0",
        "& button": {
            
            "&:first-child": {
              "flex": 1,
              "marginRight": ".5rem",
            },
        }
    }

}));

function NumberFormatCustom(props) {  //Для маски номера телефона
    const { inputRef, onChange, ...other } = props;
  
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          });
        }}
        format="+7 ### ### ## ##"
        isNumericString 
        mask="_"     
          
      />
    );
  }

  

function Signup(props) {
    const classes = useStyles();
    let theme = useTheme();

    const [values, setValues] = React.useState({ //Для маски номера телефона
        numberformat: '',
      });
    
    const handleChange2 = (event) => { //Для маски номера телефона
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
    };

    theme = createMuiTheme({
        palette: {
          primary: {
            main: "#f47522",
            dark: "#f47522", 
            contrastText: "#fff",       
          },
          secondary: {
            main: "#696983",
            contrastText: "#fff",
          },
        }
      });

    return(
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <div className={classes.leftCol}>
                    <div className={classes.wrapper}>
                        <div className={classes.userCard}> 
                            <Logo />
                            <div>
                                <FormControl component="fieldset" fullWidth> 
                                    

                                    <TextField
                                        label="Номер телефона"
                                        value={values.numberformat}
                                        onChange={handleChange2}
                                        name="numberformat"
                                        fullWidth
                                        variant="outlined"
                                        className={classes.telNoField}
                                        id="formatted-numberformat-input"
                                        InputProps={{
                                            inputComponent: NumberFormatCustom,                    
                                        }}
                                        />
                                     
                                    <TextField id="pass" label="Пароль" type="password" variant="outlined" fullWidth />
                                    
                                    
                                    <p className={classes.buttons}>
                                                <Button variant="contained" color="primary">
                                                    Вход
                                                </Button>
                                                <Button variant="outlined" color="primary">
                                                    Забыли пароль?
                                                </Button>
                                    </p>
                                    <Button to="/signup" variant="outlined" component={Link} color="primary">
                                                    Регистрация
                                    </Button>
                                                                  
                                    
                                </FormControl>
                            </div>
                        </div>

                    </div>
                </div>
                
                    
            </div>
        </ThemeProvider>
    );
}

export default Signup;