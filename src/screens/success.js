//Успех оплаты
import React from 'react';
import { makeStyles, useTheme, createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import {Checkbox, FormGroup, FormControlLabel, Divider} from '@material-ui/core'; 
import { Link } from 'react-router-dom';




import 'fontsource-roboto';

import Logo from'../components/forms/logo'; //Логотип Dovolen
import atmoshpere from '../assets/restaurant.svg'; //https://www.flaticon.com/authors/ultimatearm/flat
import food from '../assets/seasoning.svg'; //https://www.flaticon.com/authors/ultimatearm/flat


const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "#666",
      minHeight: "100vh",    
    },  
    wrapper: {
        width: "100%",
        [theme.breakpoints.up('sm')]: {
            width: "320px",
            padding: "1rem",

         },
    },
    
    userCard: {
        overflow: "hidden",
        display: "block",
        backgroundColor: "#fff",
        [theme.breakpoints.up('sm')]: {
            borderRadius: "1rem",           

         },
        padding: "1rem",
        "& button, .MuiFormControl-root, .MuiPaper-root": {
            margin: ".5rem 0"
        },
        "& p": {
            fontSize: ".75rem",
            textAlign: "center"
        },

    },
    links: {
        color: "#696983",
    },

    success: {
        textAlign: "center",
        margin: "1rem 0 1rem 0",
    },
    
    footer: {
        marginTop: "1.5rem",
    },
    
    checkboxIcons: {
        marginBottom: "1rem",
        justifyContent: "center",
        "& label": {
            
            moxSizing: "border-box",
            textAlign: "center",
            //margin: "1rem 0",
            flex: 1,

            
            
            "& .MuiFormControlLabel-label": {
                fontSize: ".8rem",
                color: "#666",
                display: "block",
                width: "5rem",
                padding: "0 0 .5rem 0",               
                
            },
            "& .MuiIconButton-root": {  
                width: "5rem",
                height: "5rem",
                padding: ".5rem .25rem",                
                backgroundSize: "contain",
                backgroundPosition: "top center",
                backgroundRepeat: "no-repeat",
                backgroundColor: "rgba(255,255,255,0) !important",
                borderRadius: 0,
                opacity: .5,
                filter: "grayscale(1)",
                "& span": {
                    display: "none",
                }
            },
            "& .Mui-checked": {
                opacity: "1 !important",
                filter: "grayscale(0) !important",
            }

        }
    },
    atmoshpere: {        
        "& .MuiIconButton-root": {                     
            backgroundImage: "url("+atmoshpere+")",            
        },        
    },
    food: {
        "& .MuiIconButton-root": {
            backgroundImage: "url("+food+")",
        },        
    },    


}));




function Success(props) {
    const classes = useStyles();
    let theme = useTheme();
    

    theme = createMuiTheme({
        palette: {
          primary: {
            main: "#f47522",
            dark: "#f47522", 
            contrastText: "#fff",       
          },
          secondary: {
            main: "#696983",
            contrastText: "#fff",
          },
        }
      });

    return(
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <div className={classes.wrapper}>
                    <div className={classes.userCard}>
                        <Logo />
                        <Divider />
                        <div className={classes.success}>
                            Чаевые успешно отправлены!
                        </div>
                        <FormGroup aria-label="position" row className={classes.checkboxIcons}>
                                        <FormControlLabel
                                            value="atmoshpere"
                                            control={<Checkbox color="primary" />}
                                            label={"Понравилась атмосфера?"}
                                            labelPlacement="bottom"
                                            className={classes.atmoshpere}
                                        />
                                        <FormControlLabel
                                            value="food"
                                            control={<Checkbox color="primary" />}
                                            label="Понравилась кухня?"
                                            labelPlacement="bottom"
                                            className={classes.food}
                                        />


                        </FormGroup>
                        
                        
                        
                        <Divider />
                        <footer className={classes.footer}>                            
                            
                            <p><Link to=""  className={classes.links}>Контакты</Link></p>
                            <p><Link to=""  className={classes.links}>Получателям чаевых и&nbsp;заведениям</Link></p>
                        </footer>

                    </div>

                </div>
                    
            </div>
        </ThemeProvider>
    );
}

export default Success;