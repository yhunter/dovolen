//Форма оплаты
import React, { useState } from 'react';
import { makeStyles, useTheme, createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import {Avatar, Typography, TextField, Button, Checkbox, Divider, FormControlLabel, Chip} from '@material-ui/core'; 
import { Link } from 'react-router-dom';
import Rating from '@material-ui/lab/Rating';

import 'fontsource-roboto';

import Logo from'../components/forms/logo'; //Логотип Dovolen
import ava from '../assets/courtney-cook-le7D9QFiPr8-unsplash.jpg';
import gpay from '../assets/gpay.svg';
import applepay from '../assets/applepay.svg';



function Pay(props) {
    const classes = useStyles();
    let theme = useTheme();

    const [commentsClass, showCommentClass] = useState(''); //Состояние элемента скрытого блока с комментариями. Открывается при клике на рейтинг (добавляем класс commentsShow)
    const onRatingClick = () => { //Клик по рейтингу
        showCommentClass(' commentsShow');        
    };

    
    const [tip, plusTip] = useState(0); //Состояние для отображения суммы чаевых и обработки кликов по чипам добавлениям суммы
    const easyTips = [100, 200, 500, 1000]; //Массив для отрисовки чипов добавлениям суммы
    const easyTipClick = (item) => {//Клик по чипу        
        plusTip(tip+item);
    }
    const tipChange = (item) => {//Изменение поля чаевых
        plusTip(parseInt(item.target.value));
    }

    const [leaderClass, showLeaderClass] = useState(''); //Состояние элемента скрытого блока программы лидера. Открывается при клике не чекбоксе (добавляем класс commentsShow)
    const leaderClassShow = () => { //Изменение чекбокса
       
        showLeaderClass(' commentsShow');
        
    }
     

    theme = createMuiTheme({
        palette: {
          primary: {
            main: "#f47522",
            dark: "#f47522", 
            contrastText: "#fff",       
          },
          secondary: {
            main: "#696983",
            contrastText: "#fff",
          },
        }
      });

    return(
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <div className={classes.wrapper}>
                    <div className={classes.userCard}>
                        <Logo />
                        <Divider />
                        <div  className={classes.userData}>
                            <Avatar alt="Василиса Константинопольская" src={ava} className={classes.userPic} />
                            <div className={classes.userName}>
                                <Typography variant="subtitle1">Василиса Константинопольская</Typography>
                                Официант, ресторан «Рандеву»
                                <Typography variant="subtitle2">Коплю на машину. Осталось немножко — 50&nbsp;т.р. 🚗</Typography>
                            </div>
                        </div>
                        <TextField id="tip" label="Сумма, ₽" type="number" value={tip} onChange={(e) => {tipChange(e);}} inputProps={{min: "0", step: "10" }} variant="outlined" fullWidth />
                        <div className={classes.chips}>
                            {easyTips.map((item, index) => (
                                <Chip
                                    size="small"
                                    label={"+ "+item}
                                    clickable
                                    key={"badge"+index}
                                    onClick={
                                        () => {easyTipClick(item);}
                                    }
                                />    

                            ))}
                        </div>
                        <div className={classes.rating}>
                                    <Rating 
                                        name="size-medium"
                                        defaultValue={0}
                                        onChange={(event, newValue) => {
                                            onRatingClick();                                            
                                        }}
                                    />
                        </div>
                        
                        <div className={classes.commentsWrapper} >
                            <div className={classes.comments + commentsClass}>
                                    <TextField
                                        id="guestResponse"
                                        label="Ваш отзыв"
                                        multiline
                                        fullWidth
                                        rows={5}
                                        defaultValue=""
                                        variant="outlined"
                                    />
                                    <FormControlLabel                            
                                        
                                        control={<Checkbox color="primary" id="anonymusResponse" />}
                                        label="Непубличный"               
                                    />
                                    
                                    
                                    <p style={{"textAlign": "center"}}>
                                        <Button variant="outlined" color="primary">
                                            Оставить только отзыв
                                        </Button>
                                    </p>
                                    <Divider />
                            </div>
                        </div>
                        <p><b>Понравился сотрудник? Зарегистрируйтесь и напишите ему личное сообщение:</b></p>
                        <TextField
                                        id="guestEmail"
                                        label="Ваш email для авторегистрации"                                        
                                        fullWidth
                                        defaultValue=""
                                        variant="outlined"
                                        type="email"
                        />                   
                        <Button variant="contained" color="primary" fullWidth>
                            Оплатить Картой
                        </Button>
                        <Button variant="outlined" color="secondary" fullWidth>
                            <img src={gpay} alt="GPay" className={classes.buttonIcon} />
                        </Button>
                        <Button variant="outlined" color="secondary" fullWidth>
                            <img src={applepay} alt="Apple Pay" className={classes.buttonIcon} />
                        </Button>
                        
                        <div className={classes.costs+ " " +classes.leaderLink}>
                            <FormControlLabel                        
                                control={<Checkbox color="primary" id="costsStatus" />}
                                label={
                                        <>
                                            Я хочу взять на себя транзакционные издержки (5%),<br /> чтобы сотрудник получил полную сумму
                                        </>
                                }               
                            /> 
                        </div>
                        <div className={classes.leader}>
                            <b>Лидер благодарности:</b> Константин Константинопольский — &nbsp;2500&nbsp;₽
                        </div> 
                        <div className={classes.leaderLink}>
                            <FormControlLabel                        
                                control={<Checkbox color="primary" id="guestLeaderStatus" onChange={leaderClassShow} />}
                                label={
                                        <>
                                            Участвовать в программе <nobr>«<Link to={'/leader'}>Лидер благодарности</Link>»</nobr>
                                        </>
                                }               
                            /> 
                            <div className={classes.commentsWrapper} >
                                <div className={classes.comments + leaderClass}>
                                    <TextField
                                            id="gueastName"
                                            label="ФИО"                                        
                                            fullWidth
                                            defaultValue=""
                                            variant="outlined"
                                            type="text"
                                        />
                                </div>
                            </div>    
                        </div> 
                        
                        <Divider />
                        
                        <footer className={classes.footer}>
                            <p>
                            Совершая платеж, вы соглашаетесь с&nbsp;<Link to=""  className={classes.links} >условиями&nbsp;сервиса</Link>.</p>
                            <p><Link to=""  className={classes.links}>Условия использования</Link></p>
                            <p><Link to=""  className={classes.links}>Политика конфиденциальности</Link></p>
                            <p><Link to=""  className={classes.links}>Контакты</Link></p>
                            <p><Link to=""  className={classes.links}>Получателям чаевых и&nbsp;заведениям</Link></p>
                        </footer>

                    </div>

                </div>
                    
            </div>
        </ThemeProvider>
    );
}

export default Pay;

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      alignItems: "center",
      justifyContent: "center",
      //backgroundColor: "#666",
      backgroundColor: "#696983",
      background: "linear-gradient(180deg, rgba(157,157,181,1) 0%, rgba(105,105,131,1) 100%)",
      minHeight: "100vh",    
    },  
    wrapper: {
        width: "100%",
        [theme.breakpoints.up('md')]: {
            width: "35%",
            minWidth: "320px",
            padding: "1rem",

         },
    },
    userCard: {
        overflow: "hidden",
        display: "block",
        backgroundColor: "#fff",
        padding: "1rem",
        [theme.breakpoints.up('md')]: {
            borderRadius: "1rem",           
            boxShadow: "0 0 1rem rgba(30,30,100, .3)",
            padding: "2rem",
         },
        
        "& button, .MuiFormControl-root, .MuiPaper-root": {
            margin: ".5rem 0"
        },
        "& p": {
            fontSize: ".75rem",
            textAlign: "center"
        },

    },
    links: {
        color: "#696983",
    },
    userPic: {
        width: "100px",
        height: "100px",
        margin: "1rem .75rem 1rem 0",
    },
    userData: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    userName: {
        fontSize: ".65rem",
        "& .MuiTypography-subtitle1": {
            fontWeight: "bold",
            lineHeight: 1.25,
            
        },
        
        "& .MuiTypography-subtitle2": {
            marginTop: ".5rem",
            lineHeight: 1.25,
        },
    },
    heading: {
        fontSize: "1rem !important",
        color: "#696983",
    },
    answer: {
        flexDirection: "column",
    },
    rating: {
        textAlign: "center",
        margin: ".5rem",
    },
    footer: {
        marginTop: "1.5rem",
    },
    comments: {
        maxHeight: "0px",
        overflow: "hidden",
        transition: "max-height .5s ease-out 0s",
        display: "block",
        position: "relative",
        textAlign: "center",
        "& .MuiFormControlLabel-label": {
            fontSize: ".8rem",
        }
        
    },
    commentsWrapper: {
        "& .commentsShow": {
            maxHeight: "500px",

        }
    },
    checkboxIcons: {
        justifyContent: "center",
        "& label": {
            maxWidth: "4rem",
            moxSizing: "border-box",
            textAlign: "center",
            
            
            "& .MuiFormControlLabel-label": {
                fontSize: ".8rem",
                color: "#666",
                display: "block",
                width: "5rem",
                padding: "0 0 .5rem 0",               
                
            },
            "& .MuiIconButton-root": {  
                width: "3rem",
                height: "3rem",
                padding: ".5rem .25rem",                
                backgroundSize: "contain",
                backgroundPosition: "top center",
                backgroundRepeat: "no-repeat",
                backgroundColor: "rgba(255,255,255,0) !important",
                borderRadius: 0,
                opacity: .5,
                filter: "grayscale(1)",
                "& span": {
                    display: "none",
                }
            },
            "& .Mui-checked": {
                opacity: "1 !important",
                filter: "grayscale(0) !important",
            }

        }
    },
    chips: {
        textAlign: "center",
        "& .MuiChip-clickable": {
            margin: "0 .15rem",
        }
    },
    costs: {
        margin: "1rem 0 1rem 0 !important", 
        paddingBottom: "0 !important",
    },
    leader: {
        textAlign: "center",
        fontSize: ".8rem",
        margin: "0rem 0 .5rem 0",
        paddingBottom: ".5rem",
        //borderBottom: "1px #ccc solid",
        "& a": {
            color: "#696983"
        },
        "& b": {
            display: "block",
        },
    },
    leaderLink: {
        fontSize: ".8rem",
        textAlign: "center",
        display: "block",
        margin: "0 auto",
        paddingBottom: "1.5rem",
        "& label": {
            display: "inline",
            margin: 0,
            "& .MuiIconButton-root": {
                padding: "0",
                marginRight: ".5rem",
            }
        },
        "& a": {
            color: "#696983"
        },
        "& .MuiFormControlLabel-label": {
            width: "11rem",
            fontSize: ".8rem",
            display: "inline",
            
        }
    },
    buttonIcon: {
        height: "1.5rem",
    }


}));