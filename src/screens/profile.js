//Личный кабинет
import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import {  createMuiTheme, makeStyles, useTheme, ThemeProvider } from '@material-ui/core/styles';
import {AppBar, CssBaseline, Drawer, Hidden, Toolbar, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import 'fontsource-roboto';

import User from '../components/profile/user.js'; //Аватарка в аппбаре с меню
import SideMenu from '../components/profile/sideMenu.js'; //Меню 

import menuItems from './profile/menuItems.js'; //Импорт данных о разделах и содержимом меню

const color = {};

const drawerWidth = 240; //Ширина боковой панели

const pageSelector = (page="main", role="profile") => { // Выбираем контент для показа в основной области 
  let component = menuItems(role)[menuItems("role")[role].menu1[0]].component; // В любой непонятной ситуации берем первый пункт из меню

  if (typeof menuItems(role)[page] !== 'undefined') {
      component = menuItems(role)[page].component;
  }
  return component;
}

const roleSelector = (page = 'main', url = '/profile') => { //Заглушка для определения роли пользователя по маршруту
  //отдаем роли: recepient, client, manager, agent
  let role = "recepient"; //По-умолчанию - получатель чаевых
  role = url.replace(page, "").replace(/\//g, ""); //Вычитаем из маршрута адрес страницы, убираем слеши  
  return role;
}


function Profile(props) {   

  const { window } = props;
  const classes = useStyles();
  let theme = useTheme();

  
  let role = roleSelector(props.match.params.page, props.match.url); //Определяем роль для передачи в дочерние компоненты 
  let page = menuItems("role")[role].menu1[0]; //Задаем дефолтный подраздел (первый для данной роли из menuItems)
  const menu = menuItems(role);

  if (typeof (props.match.params.page)!='undefined') {
    if (typeof menu[props.match.params.page] !== "undefined") {
      page = props.match.params.page; //Берем подраздел из переданных параметров   
    }    
  }  
 

  theme = createMuiTheme({
    palette: {
      primary: {
        main: "#f47522",
        dark: "#f47522", 
        contrastText: "#fff",       
      },
      secondary: {
        main: "#696983",
        contrastText: "#fff",
      },
    },
    mixins: {
      toolbar: {
        backgroundColor: "#fff",
      }
    },
  });
  
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div> {/* Боковое меню */}
      <SideMenu list={menu} selected={page} role={role} handle={()=> setMobileOpen(false)} /> 
    </div>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

    
  return (
    <ThemeProvider theme={theme}>
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed"  elevation={0} className={classes.appBar} >
        <Toolbar className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap className={classes.appBarHeader}> {/* Заголовок страницы */}
            {menu[page].name}
          </Typography>

          <User user="000123" role={role} />
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}  aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      
      <main className={classes.content}> {/* Основной контент */}
        <div className={classes.toolbar} />
        {pageSelector(props.match.params.page, role)}  {/* Выбираем контент для показа в основной области */}  
      </main>
    </div>
    </ThemeProvider>
  );
}

export default Profile;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',    
  },
  a: {
    color: theme.palette.primary.main,
  },
  drawer: {
    
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    backgroundColor: color.main,
    borderBottom: "1px #ccc solid",
    color: "rgba(0, 0, 0, 0.87)",
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,      
      backgroundColor: '#fafafa',
      fontWeight: 500,
      color: color.main,      
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    backgroundColor: '#ffffff',
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  paper: {
    borderRadius: ".5rem",
  },
  logo: {
    margin: "1rem",
  },
  appBarHeader: {
    fontWeight: "bold",
    flexGrow: 1,
  },
  menu: {
    "& .Mui-selected": { 
      backgroundColor: "#696983",
      color: "#fff",
      "& svg": {
        color: "#fff",
      }, 
      "&:hover": {
        backgroundColor: "#7287a9",
      }
    },
  },
  centerText: {
    textAlign: "center", 
  }

}));