//Форма регистрации
import React from 'react';
import { makeStyles, useTheme, createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import {TextField, Button, Divider} from '@material-ui/core'; 

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';

import NumberFormat from 'react-number-format'; //для маски телефона

import 'fontsource-roboto';

import Logo from'../components/forms/logo'; //Логотип Dovolen
import bg from '../assets/formBg.jpg';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      alignItems: "center",
      justifyContent: "left",
      
      [theme.breakpoints.up('lg')]: {
        backgroundColor: "#666",  
        backgroundImage: 'url('+bg+')',
        backgroundSize: "cover",
        backgroundPosition: "50% 50%",          

      },
      
      minHeight: "100vh",    
    },  
    leftCol: {
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "100vh",

        [theme.breakpoints.up('lg')]: {
            width: "50%",             

         },
    },
    wrapper: {
        width: "100%",
        [theme.breakpoints.up('lg')]: {
            width: "320px",
            padding: "1rem",

         },
    },

    userCard: {
        overflow: "hidden",
        display: "block",
        backgroundColor: "#fff",
        
        [theme.breakpoints.up('lg')]: {
            borderRadius: "1rem",
            boxShadow: ".5rem .5rem 1rem rgba(30,30,30,.5)",           
         },
        padding: "1rem",
        "& button, .MuiFormControl-root, .MuiPaper-root": {
            margin: ".5rem 0"
        },
        "& p": {
            fontSize: ".75rem",
            textAlign: "center"
        },

    },

    
    userData: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
    },
  


}));

function NumberFormatCustom(props) {  //Для маски номера карты
    const { inputRef, onChange, ...other } = props;
  
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          });
        }}
        format="+7 ### ### ## ##"
        isNumericString 
        mask="_"     
          
      />
    );
  }

  

function Signup(props) {
    const classes = useStyles();
    let theme = useTheme();

    const [values, setValues] = React.useState({ //Для маски номера карты
        numberformat: '',
      });
    
    const handleChange2 = (event) => { //Для маски номера карты
        setValues({
          ...values,
          [event.target.name]: event.target.value,
        });
    };

    const [value, setValue] = React.useState('recipient'); //Радио инпут для выбора роли

    const handleChange = (event) => { ///Радио инпут для выбора роли
      setValue(event.target.value);
    };

    const [checked, setChecked] = React.useState(false); //Чекбокс согласия с условиями

    const checkboxChange = (event) => { //Чекбокс согласия с условиями
        setChecked(event.target.checked);
    };

    theme = createMuiTheme({
        palette: {
          primary: {
            main: "#f47522",
            dark: "#f47522", 
            contrastText: "#fff",       
          },
          secondary: {
            main: "#696983",
            contrastText: "#fff",
          },
        }
      });

    return(
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <div className={classes.leftCol}>
                    <div className={classes.wrapper}>
                        <div className={classes.userCard}> 
                            <Logo />
                            <Divider />
                            <div>
                                <FormControl component="fieldset" fullWidth> 
                                    <RadioGroup aria-label="role" name="role" value={value} onChange={handleChange}>
                                        <FormControlLabel value="recipient" control={<Radio />} label="Получатель" />
                                        <FormControlLabel value="manager" control={<Radio />} label="Менеджер" />
                                        <FormControlLabel value="agent" control={<Radio />} label="Агент" />
                                        <FormControlLabel value="customer" control={<Radio />} label="Клиент" />
                                        
                                    </RadioGroup>

                                    <TextField
                                        label="Номер телефона"
                                        value={values.numberformat}
                                        onChange={handleChange2}
                                        name="numberformat"
                                        fullWidth
                                        variant="outlined"
                                        className={classes.telNoField}
                                        id="formatted-numberformat-input"
                                        InputProps={{
                                            inputComponent: NumberFormatCustom,                    
                                        }}
                                        />
                                     
                                    <TextField id="pass" label="Пароль" type="password" variant="outlined" fullWidth />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={checked}
                                                onChange={checkboxChange}
                                                inputProps={{ 'aria-label': 'primary checkbox' }}
                                            />
                                        }
                                        label="Принимаю условия договора оферты и&nbsp;соглашение о&nbsp;персональных данных"
                                    />
                                    
                                    <p style={{"textAlign": "center"}}>
                                                <Button variant="contained" color="primary">
                                                    Регистрация
                                                </Button>
                                    </p>
                                                                  
                                    
                                </FormControl>
                            </div>
                        </div>

                    </div>
                </div>
                
                    
            </div>
        </ThemeProvider>
    );
}

export default Signup;