import React from 'react';
import {  createMuiTheme, useTheme, ThemeProvider } from '@material-ui/core/styles';
import {AppBar, Button} from '@material-ui/core';

import { Link as ScrollLink} from 'react-scroll';


import '../assets/css/style.scss';

import About from '../components/landing/about.js'; //Первый экран
import Histories from '../components/landing/histories.js'; //Второй экран - Для кого
import HowTo from '../components/landing/howTo.js'; // Третий экран - Как работает сервис
import Features from '../components/landing/features.js'; // Четвертый экран - почему выбирают?
import Integration from '../components/landing/integration.js'; // Пятый экран - интеграции в фирменный стиль
import Price from '../components/landing/price.js'; // Шестой экран - тарифы
import LandingFooter from '../components/landing/footer.js'; // Подвал

import Histories2 from '../components/landing/histories2.js'; //Второй экран - Для кого, с убраными историями - ссылки ведут на "почему выбирают"

import logo from'../assets/logo.svg'; //Логотип Dovolen

import ArrowForwardRoundedIcon from '@material-ui/icons/ArrowForwardRounded';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';



export default function Landing(props) {

    var root = document.querySelector(":root"); //Задаем класс для <html> лендинга
    root.classList.add("front");

    let theme = useTheme();

    theme = createMuiTheme({
        palette: {
          primary: {
            main: "#f47522",
            dark: "#f47522", 
            contrastText: "#fff",       
          },
          secondary: {
            main: "#696983",
            contrastText: "#fff",
          },
        },
        mixins: {
          toolbar: {
            backgroundColor: "#fff",
          }
        },
      });


    
    return (
        <ThemeProvider theme={theme}>
            <AppBar position="fixed"  elevation={0} className={"appBar"} >
                
                <div className="logo">
                  <a href="/" title="«dovolen» — безналичная благодарность">
                    <img src={logo} alt="dovolen" />
                  </a>
                </div>
                
                <div className="menu">
                  <nav>
                    <ScrollLink to="business" smooth={true} offset={-100} duration={500} >Бизнесу</ScrollLink>                    
                    <ScrollLink to="blogers" smooth={true} offset={-100} duration={500} >Блогерам</ScrollLink>
                    <ScrollLink to="agents" smooth={true} offset={-100} duration={500} >Агентам</ScrollLink>
                    {/* <ScrollLink to="blogers" smooth={true} offset={-100} duration={500} >Блогерам</ScrollLink>*/}
                  </nav>
                </div>
                {/* 
                <div className="form">
                  
                  <form>
                    <div className="thanks">
                      Поблагодарить:
                    </div>
                    
                    <InputBase                     
                      className="codeInput"
                      placeholder={'Код сотрудника'}
                      endAdornment={
                        <IconButton size="small">
                          <ArrowForwardRoundedIcon />
                        </IconButton>
                      }
                    />
                  </form> 
                  </div>
                  */} 
                                                  
                
                <div className="login">
                    <Button variant="contained" color="primary">Вход</Button>
                </div> 
               
                
            </AppBar>
            <div className="container landing">
                
                <form className="subHeader">
                    <div className="thanks">
                      Поблагодарить:
                    </div>
                    {/* <TextField id="payForm" label="Код сотрудника" size="small" variant="outlined" />*/}
                    <InputBase                     
                      className="codeInput"
                      placeholder={'Код сотрудника'}
                      endAdornment={
                        <IconButton size="small">
                          <ArrowForwardRoundedIcon />
                        </IconButton>
                      }
                    />
                  </form> 
                     
                <About />
                
                {/* <Histories /> меняем вариант со слайдером на ссылки для перехода на "почему выгодно" */}  
                <Histories2 />
                    
                <HowTo />

                <Features />
                
                <Integration />
                
                <Price />          

                <ScrollLink to="about" className="scrollTop" smooth={true} offset={-150} duration={500} title="Вверх!" >&#8679;</ScrollLink>   

                <LandingFooter />
                
            </div>
        </ThemeProvider>
    )
    
}
